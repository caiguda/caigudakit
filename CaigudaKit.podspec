#
# Be sure to run `pod lib lint CaigudaKit.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|

    #root
        s.name      = 'CaigudaKit'
        s.version   = '1.1.6'
        s.summary   = 'CaigudaKitTest descriptions'
        s.license   = { :type => 'Apache', :file => 'LICENSE.txt' }

        s.homepage  = 'http://caiguda.com'
        s.authors   = {'Caiguda Software Studio' => 'info@caiguda.com'}
        s.homepage  = 'http://caiguda.com'
        s.source   = { :git => 'https://bitbucket.org/caiguda/caigudakit.git', :tag => '1.1.6' }

    #platform
        s.platform = :ios
        s.ios.deployment_target = '7.0'
  
    #build settings
        s.requires_arc = true

    #file patterns		
        s.frameworks = 'CoreData'
        s.frameworks   = 'QuartzCore'
		
        s.dependency 'MBProgressHUD'
        s.dependency 'ODRefreshControl'
        s.dependency 'BlocksKit'
		    s.dependency 'AFNetworking', '~> 1.3'
		    s.dependency 'Reachability'
        
        s.public_header_files = 'CaigudaKit/**/*.h'
        s.source_files = 'CaigudaKit/**/*.{h,m}'
        
end
