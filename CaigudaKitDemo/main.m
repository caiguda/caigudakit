//
//  main.m
//  CaigudaKitDemo
//
//  Created by Malaar on 11.02.14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CTAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([CTAppDelegate class]));
    }
}
