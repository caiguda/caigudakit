//
//  CTPagingMoreCellProtocol.h
//  CaigudaKit
//
//  Created by Malaar on 20.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CTPagingMoreCellProtocol <NSObject>

- (void)didBeginDataLoading;
- (void)didEndDataLoading;

@end

@protocol CTPagingMoreCellDataProtocol <NSObject>

@optional
- (void)addTarget:(id)aTarget action:(SEL)anAction;

@end