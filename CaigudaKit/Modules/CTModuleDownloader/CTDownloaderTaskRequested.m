//
//  CTDownloaderTask.m
//  CaigudaKit
//
//  Created by Malaar on 24.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTDownloaderTaskRequested.h"

@interface CTDownloaderTaskRequested ()

- (void)informDelegateAboutDownloadBytes:(long long)aBytes fromTotalBytes:(long long)aTotalBytes;
- (void)informDelegateAboutCompleteWithSuccess:(BOOL)aSuccess;

@end


@implementation CTDownloaderTaskRequested

@synthesize request;

- (id)initWithRequest:(CTGatewayRequest*)aRequest
{
    self = [super init];
    if(self)
    {
        request = aRequest;
        
        __weak CTDownloaderTaskRequested* __self = self;
        [request addDownloadProgressBlock:^(CTGatewayRequest *request, NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
        {
            [__self informDelegateAboutDownloadBytes:totalBytesRead fromTotalBytes:totalBytesExpectedToRead];
            
        } dispatchQueue:taskQueue];
        
        [request addResponseBlock:^(CTResponse *aResponse)
        {
            [__self requestCompleteWithResponse:aResponse];
            [__self informDelegateAboutCompleteWithSuccess:aResponse.success];
            
        } responseQueue:taskQueue];
    }
    return self;
}

- (void)dealloc
{
    [request cancel];
}

- (void)requestCompleteWithResponse:(CTResponse*)aResponse
{
    // override it
}

- (void)start
{
    [request start];
}

- (void)cancel
{
    [request cancel];
}

- (void)informDelegateAboutDownloadBytes:(long long)aBytes fromTotalBytes:(long long)aTotalBytes
{
    dispatch_async(delegateQueue, ^
    {
        [delegate task:self downloadBytes:aBytes fromTotalBytes:aTotalBytes];
    });
}

- (void)informDelegateAboutCompleteWithSuccess:(BOOL)aSuccess
{
    completedSuccess = aSuccess;
    dispatch_async(delegateQueue, ^
    {
        [delegate task:self completeWithSuccess:aSuccess];
    });
}

@end
