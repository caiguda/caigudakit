//
//  CTModuleDownloader.h
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTDownloaderTask.h"

@protocol CTModuleDownloaderDelegate;

/**
 * DAL
 * Use this module to manage downloading any data from server into your app.
 **/
@interface CTModuleDownloader : NSObject <CTDownloaderTaskDelegate>
{
    dispatch_queue_t downloaderQueue;
    void* queueTag;
    NSMutableArray* tasks;
    NSUInteger currentTaskIndex;
    BOOL originalIdleTimerDisabled;
}

/**
 * All delegate methods call in main queue
 **/
@property (nonatomic, weak) id<CTModuleDownloaderDelegate> delegate;

/**
 * Is downloading now?
 **/
@property (nonatomic, readonly) BOOL downloading;

/**
 * If you want to continue download process even if some task was failed, setup this property to YES.
 * NO by default.
 **/
@property (nonatomic, assign) BOOL continueIfTaskFailed;

/**
 * Use this field to prevent lock screen during loading. After complete downloading previous value of 'idleTimerDisabled' will be restored.
 **/
@property (nonatomic, assign) BOOL disableScreenLockDuringDownloading;

- (void)addTask:(CTDownloaderTask*)aTask;
- (NSUInteger)taskCount;

- (void)start;
- (void)cancel;

@end


@protocol CTModuleDownloaderDelegate <NSObject>

@optional

- (void)downloader:(CTModuleDownloader*)aDownloader
              task:(CTDownloaderTask*)aTask
     downloadBytes:(long long)aBytes
    fromTotalBytes:(long long)aTotalBytes;

- (void)downloader:(CTModuleDownloader*)aDownloader didStartTask:(CTDownloaderTask*)aTask atIndex:(NSUInteger)anIndex;
- (void)downloader:(CTModuleDownloader*)aDownloader didCompleteTask:(CTDownloaderTask*)aTask withSuccess:(BOOL)aSuccess;
- (void)downloader:(CTModuleDownloader*)aDownloader didCompleteWithResult:(BOOL)aSuccess;

@end