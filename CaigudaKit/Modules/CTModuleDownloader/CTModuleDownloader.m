//
//  CTModuleDownloader.m
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTModuleDownloader.h"
#import "CTKitDefines.h"

@interface CTModuleDownloader ()

- (void)startTaskAtIndex:(NSUInteger)anIndex;

- (void)informDelegateAboutCompleteWithSuccess:(BOOL)aSuccess;
- (void)informDelegateAboutStartTask:(CTDownloaderTask*)aTask atIndex:(NSUInteger)anIndex;
- (void)informDelegateAboutCompleteTask:(CTDownloaderTask*)aTask withSuccess:(BOOL)aSuccess;

@end

@implementation CTModuleDownloader

@synthesize downloading;

#pragma mark - Init/Dealloc

- (id)init
{
    self = [super init];
    if(self)
    {
        tasks = [NSMutableArray new];
        downloaderQueue = dispatch_queue_create("com.caiguda.CTModuleDownloader", NULL);
        queueTag = &queueTag;
        dispatch_queue_set_specific(downloaderQueue, queueTag, queueTag, NULL);
    }
    return self;
}

- (void)dealloc
{
#if CT_NEEDS_DISPATCH_RETAIN_RELEASE
    if (downloaderQueue)
        dispatch_release(downloaderQueue);
#endif
    downloaderQueue = NULL;
}

- (void)addTask:(CTDownloaderTask*)aTask
{
    NSParameterAssert(aTask);
    
    dispatch_async(downloaderQueue, ^
    {
        [tasks addObject:aTask];
        [aTask setDelegate:self withDispatchQueue:downloaderQueue];
    });
}

- (NSUInteger)taskCount
{
    return [tasks count];
}

- (void)start
{
	NSAssert(!dispatch_get_specific(queueTag), @"CTModuleDownloader: Invoked on incorrect queue");
    NSAssert(self.delegate, @"CTModuleDownloader: setup delegate");
    
    originalIdleTimerDisabled = [UIApplication sharedApplication].idleTimerDisabled;
    [UIApplication sharedApplication].idleTimerDisabled = self.disableScreenLockDuringDownloading;

    dispatch_async(downloaderQueue, ^
    {
        if(tasks.count)
        {
            currentTaskIndex = 0;
            [self startTaskAtIndex:currentTaskIndex];
            downloading = YES;
        }
        else
            [self informDelegateAboutCompleteWithSuccess:NO];
    });
}

- (void)cancel
{
    void(^cancelBlock)(void) = ^(void)
    {
        for(CTDownloaderTask* task in tasks)
            [task cancel];
        currentTaskIndex = 0;
        downloading = NO;
    };
    if(dispatch_get_specific(queueTag))
        cancelBlock();
    else
        dispatch_async(downloaderQueue, cancelBlock);
}

- (void)startTaskAtIndex:(NSUInteger)anIndex
{
	NSAssert(dispatch_get_specific(queueTag), @"CTModuleDownloader: Invoked on incorrect queue");
    
    CTDownloaderTask* task = [tasks objectAtIndex:currentTaskIndex];
    [task start];
    [self informDelegateAboutStartTask:task atIndex:anIndex];
}

#pragma mark - Inform delegate

- (void)informDelegateAboutCompleteWithSuccess:(BOOL)aSuccess
{
    dispatch_async(dispatch_get_main_queue(), ^
    {
        [UIApplication sharedApplication].idleTimerDisabled = originalIdleTimerDisabled;

        if([self.delegate respondsToSelector:@selector(downloader:didCompleteWithResult:)])
            [self.delegate downloader:self didCompleteWithResult:aSuccess];
    });
}

- (void)informDelegateAboutStartTask:(CTDownloaderTask*)aTask atIndex:(NSUInteger)anIndex
{
    dispatch_async(dispatch_get_main_queue(), ^
    {
        if([self.delegate respondsToSelector:@selector(downloader: didStartTask:atIndex:)])
            [self.delegate downloader:self didStartTask:aTask atIndex:anIndex];
    });
}

- (void)informDelegateAboutCompleteTask:(CTDownloaderTask*)aTask withSuccess:(BOOL)aSuccess
{
    dispatch_async(dispatch_get_main_queue(), ^
    {
        if([self.delegate respondsToSelector:@selector(downloader:didCompleteTask:withSuccess:)])
            [self.delegate downloader:self didCompleteTask:aTask withSuccess:aSuccess];
    });
}

#pragma mark - CTDownloaderTaskDelegate

- (void)task:(CTDownloaderTask*)aTask downloadBytes:(long long)aBytes fromTotalBytes:(long long)aTotalBytes
{
    dispatch_async(dispatch_get_main_queue(), ^
    {
        if([self.delegate respondsToSelector:@selector(downloader:task:downloadBytes:fromTotalBytes:)])
            [self.delegate downloader:self task:aTask downloadBytes:aBytes fromTotalBytes:aTotalBytes];
    });
}

- (void)task:(CTDownloaderTask*)aTask completeWithSuccess:(BOOL)aSuccess
{
    [self informDelegateAboutCompleteTask:aTask withSuccess:aSuccess];

    if(!aSuccess && !_continueIfTaskFailed)
    {
        [self cancel];
        [self informDelegateAboutCompleteWithSuccess:NO];
        return;
    }

    ++currentTaskIndex;
    if(currentTaskIndex < tasks.count)
    {
        [self startTaskAtIndex:currentTaskIndex];
    }
    else
    {
        downloading = NO;

        BOOL result = YES;
        for(CTDownloaderTask* task in tasks)
            result &= task.completedSuccess;
        [self informDelegateAboutCompleteWithSuccess:result];
    }

}

@end
