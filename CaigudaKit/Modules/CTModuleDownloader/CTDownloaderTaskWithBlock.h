//
//  CTDownloaderTaskWithBlock.h
//  CaigudaKit
//
//  Created by Malaar on 24.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTDownloaderTask.h"
#import "CTResponse.h"

@class CTDownloaderTaskWithBlock;
typedef void (^CTDownloaderTaskBlock) (CTDownloaderTaskWithBlock* aTask);

/**
 * DAL
 * This block will execute in taskQueue (You must execute it only in queue taskQueue!).
 **/
@interface CTDownloaderTaskWithBlock : CTDownloaderTask

@property (nonatomic, copy) CTDownloaderTaskBlock block;

- (void)taskCompleteWithSuccess:(BOOL)aSuccess;

@end
