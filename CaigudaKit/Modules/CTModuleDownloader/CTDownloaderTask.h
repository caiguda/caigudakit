//
//  CTDownloaderTask.h
//  CaigudaKit
//
//  Created by Malaar on 24.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CTDownloaderTaskDelegate;

/**
 * DAL
 * Downloader task - use for process unit of download work.
 **/
@interface CTDownloaderTask : NSObject
{
    dispatch_queue_t taskQueue;

    dispatch_queue_t delegateQueue;
    
    __weak id<CTDownloaderTaskDelegate> delegate;
    BOOL completedSuccess;
}

@property (nonatomic, strong) NSString* identifier;
@property (nonatomic, readonly) BOOL completedSuccess;

@property (nonatomic, readonly) dispatch_queue_t delegateQueue;

- (void)setDelegate:(id<CTDownloaderTaskDelegate>)aDelegate withDispatchQueue:(dispatch_queue_t)aDispatchQueue;

- (void)start;
- (void)cancel;

@end

/**
 * Use only for CTModuleDownloader
 **/
@protocol CTDownloaderTaskDelegate <NSObject>

- (void)task:(CTDownloaderTask*)aTask downloadBytes:(long long)aBytes fromTotalBytes:(long long)aTotalBytes;
- (void)task:(CTDownloaderTask*)aTask completeWithSuccess:(BOOL)aSuccess;

@end