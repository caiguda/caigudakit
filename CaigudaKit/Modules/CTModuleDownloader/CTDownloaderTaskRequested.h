//
//  CTDownloaderTask.h
//  CaigudaKit
//
//  Created by Malaar on 24.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTDownloaderTask.h"
#import "CTGatewayRequest.h"

/**
 * DAL
 * Downloasd task with request
 **/
@interface CTDownloaderTaskRequested : CTDownloaderTask

@property (nonatomic, readonly) CTGatewayRequest* request;

- (id)initWithRequest:(CTGatewayRequest*)aRequest;

/**
 * Process response here (this method called in queue taskQueue)
 * Override it in your subclass
 **/
- (void)requestCompleteWithResponse:(CTResponse*)aResponse;

@end
