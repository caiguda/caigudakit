//
//  CTModuleRemotePushes.m
//  CaigudaKit
//
//  Created by Malaar on 29.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTModuleRemotePushes.h"

//#define kCTModuleRemotePushesDeviceToken    @"kCTModuleRemotePushesDeviceToken"
//#define kCTModuleRemotePushesLastUpdate     @"kCTModuleRemotePushesLastUpdate"

@implementation CTModuleRemotePushes

@synthesize deviceToken;
//@dynamic deviceTokenLastUpdate;

#pragma mark - Defaults

//- (NSString*)deviceToken
//{
//    return [[NSUserDefaults standardUserDefaults] objectForKey:kCTModuleRemotePushesDeviceToken];
//}
//
//- (void)setDeviceToken:(NSString*)aDeviceToken
//{
//    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//    if(aDeviceToken)
//        [defaults setObject:aDeviceToken forKey:kCTModuleRemotePushesDeviceToken];
//    else
//        [defaults removeObjectForKey:kCTModuleRemotePushesDeviceToken];
//
//    [defaults synchronize];
//}
//
//- (NSDate*)deviceTokenLastUpdate
//{
//    return [[NSUserDefaults standardUserDefaults] objectForKey:kCTModuleRemotePushesLastUpdate];
//}
//
//- (void)setDeviceTokenLastUpdate:(NSDate *)aDeviceTokenLastUpdate
//{
//    NSUserDefaults* defaults = [NSUserDefaults standardUserDefaults];
//    if(aDeviceTokenLastUpdate)
//        [defaults setObject:aDeviceTokenLastUpdate forKey:kCTModuleRemotePushesLastUpdate];
//    else
//        [defaults removeObjectForKey:kCTModuleRemotePushesLastUpdate];
//    
//    [defaults synchronize];
//}
//
//- (BOOL)shouldUpdateDeviceToken
//{
//    return self.deviceTokenUpdateTimeInterval != 0 && abs([self.deviceTokenLastUpdate timeIntervalSinceNow]) > self.deviceTokenUpdateTimeInterval;
//}

#pragma mark - Device token Register/Update/Unregister

- (void)tryToRegisterForRemoteNotificationTypes:(UIRemoteNotificationType)aRemoteNotificationTypes
{
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:aRemoteNotificationTypes];
    
//    if(!self.deviceToken)
//    {
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:aRemoteNotificationTypes];
//    }
//    else
//    {
//        if([self shouldUpdateDeviceToken])
//        {
//            CTRequest* updateRequest = [self updateDeviceTokenRequest];
//            [updateRequest addResponseBlock:^(CTResponse *aResponse)
//            {
//                if(aResponse.success)
//                    self.deviceTokenLastUpdate = [NSDate date];
//                
//                CTLog(@"CTModuleRemotePushes: Update with result: %i", aResponse.success);
//                
//            } responseQueue:dispatch_get_main_queue()];
//            
//            [updateRequest start];
//        }
//    }
}

- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)aDeviceToken
{
    NSString* newDeviceToken = [NSString stringWithFormat:@"%@", aDeviceToken];
    newDeviceToken = [newDeviceToken stringByReplacingOccurrencesOfString:@"<" withString:@""];
    newDeviceToken = [newDeviceToken stringByReplacingOccurrencesOfString:@">" withString:@""];
    newDeviceToken = [newDeviceToken stringByReplacingOccurrencesOfString:@" " withString:@""];
    CTLog(@"CTModuleRemotePushes: Device token: %@", newDeviceToken);
    
    deviceToken = newDeviceToken;
    
    CTRequest* registerRequest = [self registerDeviceRequestWithToken:newDeviceToken];
    if(registerRequest)
    {
        __weak __typeof(&*self) weakSelf = self;
        [registerRequest addResponseBlock:^(CTResponse *aResponse)
         {
             __strong __typeof(&*self) strongSelf = weakSelf;
             if(strongSelf)
             {
                 if(aResponse.success)
                 {
                     strongSelf->deviceToken = newDeviceToken;
                 }
                 else
                 {
                     strongSelf->deviceToken = nil;
                 }
             }
             CTLog(@"CTModuleRemotePushes: Register with result: %i", aResponse.success);
             
         } responseQueue:dispatch_get_main_queue()];
        
        [registerRequest start];
    }
}

- (void)unregisterForPushNotifications
{
    if(!self.deviceToken)
        return;
    
//    [[UIApplication sharedApplication] unregisterForRemoteNotifications];
    
    CTRequest* unregisterRequest = [self unregisterDeviceTokenRequest];
    [unregisterRequest addResponseBlock:^(CTResponse *aResponse)
    {
        CTLog(@"CTModuleRemotePushes: Unregister with result: %i", aResponse.success);
        
    } responseQueue:dispatch_get_main_queue()];

    [unregisterRequest start];
    deviceToken = nil;
}

#pragma mark - Process notification

- (void)receivePushNotification:(NSDictionary*)aNotificationInfo
{
    CTLog(@"CTModuleRemotePushes: Receive push notification: %@", aNotificationInfo);
    
    // Override this to process notification.
}

#pragma mark - Requests

- (CTRequest*)registerDeviceRequestWithToken:(NSString *)aDeviceToken
{
    NSAssert(NO, @"CTModuleRemotePushes: Override this method!");
    return nil;
}

//- (CTRequest*)updateDeviceTokenRequest
//{
//    NSAssert(NO, @"CTModuleRemotePushes: Override this method!");
//    return nil;
//}

- (CTRequest*)unregisterDeviceTokenRequest
{
    NSAssert(NO, @"CTModuleRemotePushes: Override this method!");
    return nil;
}

@end
