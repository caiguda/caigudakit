//
//  CTModuleRemotePushes.h
//  CaigudaKit
//
//  Created by Malaar on 29.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTRequest.h"

/**
 * Use subclass of this module to process remote notification.
 * To use: subclass, override appropriate methods(see below). 
 **/
@interface CTModuleRemotePushes : NSObject
{
    NSString* deviceToken;
}

@property (nonatomic, readonly) NSString* deviceToken;
//@property (nonatomic, readonly) NSDate* deviceTokenLastUpdate;

/**
 * If you use API that should update deviceToken from time to time, then 
 * setup time to update token with this property.
 * 0 by default (Don't update).
 **/
@property (nonatomic, assign) NSUInteger deviceTokenUpdateTimeInterval;

/**
 * Call it when you want to register for remote notification or want to update your registration on server.
 **/
- (void)tryToRegisterForRemoteNotificationTypes:(UIRemoteNotificationType)aRemoteNotificationTypes;

/**
 * Call it in application:didRegisterForRemoteNotificationsWithDeviceToken: method.
 **/
- (void)didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)aDeviceToken;

/**
 * Call it to unregister from remote pushes.
 **/
- (void)unregisterForPushNotifications;

/**
 * Use to process received remote push. 
 * Call it from -(BOOL)application:didFinishLaunchingWithOptions: or from -(void)application:didReceiveRemoteNotification:
 * Override this method to process received remote push notification.
 **/
- (void)receivePushNotification:(NSDictionary*)aNotificationInfo;

///**
// * checks if device token should be updated according to deviceTokenUpdateTimeInterval. Can be overriden in subclass
// **/
//- (BOOL)shouldUpdateDeviceToken;

#pragma mark - Requests
/**
 * Override this methods in your subclass
 **/
- (CTRequest*)registerDeviceRequestWithToken:(NSString*)aDeviceToken;
//- (CTRequest*)updateDeviceTokenRequest;
- (CTRequest*)unregisterDeviceTokenRequest;

@end
