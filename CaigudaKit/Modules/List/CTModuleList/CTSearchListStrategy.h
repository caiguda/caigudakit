//
//  CTSearchListStrategy.h
//  CaigudaKit
//
//  Created by Alexander Burkhai on 6/14/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTTableDisposerModeled.h"
#import "CTDataFetcher.h"
#import "CTSearchBar.h"
#import "CTSearchDisplayController.h"

#define kCTSearchListStrategySearchTextKey @"searchTextKey"

typedef enum _CTSearchBarPresentationStyle
{
    CTSearchBarPresentationStyleInContentView = 0, // default style
    CTSearchBarPresentationStyleInTableHeaderView,
} CTSearchBarPresentationStyle;

@protocol CTSearchListStrategyDelegate;

@interface CTSearchListStrategy : NSObject <UISearchDisplayDelegate, UISearchBarDelegate, CTTableDisposerDelegate, UITableViewDataSource>
{
    CTSearchBar *searchBar;
    CTSearchDisplayController *searchDisplayController;
    CTTableDisposerModeled *searchTableDisposer;        // if searchTableDisposer eq nil than strategy will use tabledisposer returned by delegate
    
    NSString *lastSearchText;
}

@property (nonatomic, readonly) CTTableDisposerModeled *tableDisposer;

@property (nonatomic, readonly) CTSearchBar *searchBar;
@property (nonatomic, readonly) CTSearchDisplayController *searchDisplayController;
@property (nonatomic, readonly) BOOL isSearchActive;

- (void)setSearchActive:(BOOL)active animated:(BOOL)animated;

/*
 * default is nil. If not empty then searchbar will be prefilled with this text every time on begin/end search
 * Note: this property conflicts with savesLastSearchText - priority for prefilled text
 */
@property (nonatomic, strong) NSString *preFilledSearchText;

/*
 * correct formatted current text (based on searchbar text) for search operation
 * Note: better user this value than searchbar.text
 */
@property (nonatomic, readonly) NSString *currentSearchText;

/*
 * by default is NO - if YES last search text will be setuped again on next willbeginsearch
 */
@property (nonatomic, assign) BOOL savesLastSearchText;

/*
 * default is NO. If YES search requests for reloading table will be sent at search button clicked action
 */
@property (nonatomic, assign) BOOL usesManualSearchStrategy;

@property (nonatomic, assign) CTSearchBarPresentationStyle searchBarPresentationStyle; // not implemented yet
@property (nonatomic, weak) id<CTSearchListStrategyDelegate> delegate;

- (void)configureWithSearchBar:(CTSearchBar *)aSearchBar
            contentsController:(UIViewController *)aContentsController;

- (void)configureWithSearchBar:(CTSearchBar *)aSearchBar
            contentsController:(UIViewController *)aContentsController
           searchTableDisposer:(CTTableDisposerModeled *)aSearchTableDisposer;

- (void)releaseViews;

- (void)cancelSearch;

@end

@protocol CTSearchListStrategyDelegate <NSObject>

@optional

- (CTTableDisposerModeled*)moduleListTableDisposerForSearchStrategy:(CTSearchListStrategy *)searchStrategy;
- (void)configureLoadedTableView:(UITableView*)aTableView forSearchStrategy:(CTSearchListStrategy *)searchStrategy;
- (NSString*)formatPreparedTextForSearchWithString:(NSString*)preparedString forSearchStrategy:(CTSearchListStrategy *)searchStrategy;

- (void)searchListStrategyWillBeginSearch:(CTSearchListStrategy *)searchStrategy;
- (void)searchListStrategyDidBeginSearch:(CTSearchListStrategy *)searchStrategy;

- (void)searchListStrategyWillEndSearch:(CTSearchListStrategy *)searchStrategy;
- (void)searchListStrategyDidEndSearch:(CTSearchListStrategy *)searchStrategy;

- (BOOL)searchListStrategy:(CTSearchListStrategy *)searchStrategy
shouldReloadTableForSearchString:(NSString *)searchString
            andSearchScope:(NSInteger)searchOption;

- (void)searchBarCancelButtonClicked:(CTSearchListStrategy *)aSearchStrategy;
- (void)searchBarSearchButtonClicked:(CTSearchListStrategy *)aSearchStrategy;

@end
