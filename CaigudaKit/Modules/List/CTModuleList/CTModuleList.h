//
//  CTModuleList.h
//  OutSpeak
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTTableDisposerModeled.h"

#import "CTPullToRefreshAdapter.h"
#import "CTActivityAdapter.h"
#import "CTDataFetcher.h"
#import "CTSearchListStrategy.h"

@class CTModuleList;
@protocol CTModuleListDelegate;
@protocol CTModuleListSearchDelegate;

typedef void (^CTModuleListFetcherFailedCallback) (CTModuleList* aModule, CTResponse* aResponse);
typedef void (^CTModuleListFetcherCantFetch) (CTModuleList* aModule, CTFetcherMessage *aMessage);

/**
 * GUI layer
 * This module just receive and show data (in one table section, by default).
 * Can use pull-to-refresh and activity Adapters.
 * You can use Compound cells with this module.
 **/
@interface CTModuleList : NSObject <CTTableDisposerModeledMulticastDelegate, CTSearchListStrategyDelegate>
{
    CTTableDisposerModeled* defaultTableDisposer;
    
    NSDate* lastUpdateDate;
    NSMutableArray* models;
    NSMutableArray* compoundModels;
    CTPullToRefreshAdapter* pullToRefreshAdapter;
    __weak id<CTModuleListDelegate> delegate;
}

@property (nonatomic, readonly) CTTableDisposerModeled* tableDisposer; // currently active datasourced tabledisposer

@property (nonatomic, strong) CTPullToRefreshAdapter* pullToRefreshAdapter;
@property (nonatomic, strong) CTActivityAdapter* activityAdapter;
@property (nonatomic, assign) NSUInteger defaultCompoundMaxModelsCount;

@property (nonatomic, strong) id<CTDataFetcher> dataFetcher;

@property (nonatomic, weak) id<CTModuleListDelegate> delegate;

@property (nonatomic, copy) CTModuleListFetcherFailedCallback fetcherFailedCallback;
@property (nonatomic, copy) CTModuleListFetcherCantFetch fetcherCantFetchCallback;

@property (nonatomic, readonly) NSDate* lastUpdateDate;

@property (nonatomic, readonly) dispatch_queue_t moduleQueue;

@property (nonatomic, readonly) NSArray* models;
@property (nonatomic, readonly) NSArray* compoundModels;

@property (nonatomic, strong) CTSearchListStrategy* searchStrategy;
@property (nonatomic, assign) BOOL isSearchEnabled;
@property (nonatomic, assign) NSInteger minSearchStringLength;
@property (nonatomic, weak) id<CTModuleListSearchDelegate> delegateSearch;


- (id)initWithTableDisposer:(CTTableDisposerModeled*)aTableDisposer;

/**
 * Call it in -(void)viewDidLoad method of controller
 **/
- (void)configureWithView:(UIView*)aView;

/**
 * Call it in -(void)viewDidUnload method of controller
 **/
- (void)releaseViews;

#pragma mark - Data fetching
- (void)reloadData;

#pragma mark - For overriding
- (void)fetchDataWithMessage:(CTFetcherMessage*)aMessage;
- (void)willFetchDataWithMessage:(CTFetcherMessage*)aMessage;
- (void)didFetchDataWithMessage:(CTFetcherMessage*)aMessage andResponse:(CTResponse*)aResponse;
- (void)saveModels:(NSArray*)aModels;

- (Class)fetcherMessageClass;
- (CTFetcherMessage*)fetcherMessage;
- (void)configureFetcherMessage:(CTFetcherMessage*)aMessage;

/**
 * You can here change array of received models.
/// * By default this method process compound models (if useCompoundCells = YES)
 **/
- (NSArray*)processFetchedModelsInResponse:(CTResponse*)aResponse;
- (void)prepareSectionsForModels:(NSArray*)aModels;
- (void)setupModels:(NSArray*)aModels forSection:(CTSectionReadonly *)aSection;

@end

@protocol CTModuleListDelegate <NSObject>

@optional
- (CTFetcherMessage*)fetcherMessageForModuleList:(CTModuleList*)aModule;
- (void)willFetchDataForModuleList:(CTModuleList*)aModule;
- (void)didFetchDataForModuleList:(CTModuleList*)aModule;
- (NSArray*)moduleList:(CTModuleList*)aModule processFetchedModelsInResponse:(CTResponse*)aResponse;
- (NSArray*)moduleList:(CTModuleList*)aModule prepareSectionsForModels:(NSArray*)aModels;
- (void)moduleList:(CTModuleList*)aModule didReloadDataWithModels:(NSArray*)aModels;

- (void)configureLoadedTableView:(UITableView*)aTableView forSearchStrategy:(CTSearchListStrategy *)searchStrategy;

@end

@protocol CTModuleListSearchDelegate <NSObject>

@optional
- (void)searchListStrategyWillBeginSearch:(CTModuleList*)aModuleList;
- (void)searchListStrategyDidBeginSearch:(CTModuleList*)aModuleList;

- (void)searchListStrategyWillEndSearch:(CTModuleList*)aModuleList;
- (void)searchListStrategyDidEndSearch:(CTModuleList*)aModuleList;

- (void)searchBarCancelButtonClicked:(CTModuleList *)aModuleList;
- (void)searchBarSearchButtonClicked:(CTModuleList *)aModuleList;


@end