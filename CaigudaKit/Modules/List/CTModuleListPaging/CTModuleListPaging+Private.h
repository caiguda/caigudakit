//
//  CTModuleListPaging+Private.h
//  OutSpeak
//
//  Created by Alexander Burkhai on 7/9/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTModuleListPaging.h"

@interface CTModuleListPaging (Private)

- (void)loadMoreData;
- (void)loadMoreDataPressed:(id)aSender;

@end
