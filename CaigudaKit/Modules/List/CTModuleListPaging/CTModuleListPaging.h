//
//  CTModuleListPaging.h
//  OutSpeak
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTModuleList.h"
#import "CTPagingConfig.h"
#import "CTPagingMoreCellProtocols.h"
#import "CTFetcherMessagePaging.h"

@protocol CTModuleListPagingDelegate;

/**
 * GUI layer
 * ModuleList with paging
 **/
@interface CTModuleListPaging : CTModuleList <CTTableDisposerMulticastDelegate>
{
    CTCell<CTPagingMoreCellProtocol>* moreCell;
}

@property (nonatomic, readonly) NSUInteger initialPageOffset;
@property (nonatomic, readonly) BOOL itemsAsPage;
@property (nonatomic, assign) NSUInteger pageSize;
@property (nonatomic, assign) CTLoadMoreDataType loadMoreDataType;

@property (nonatomic, readonly) BOOL reloading;
@property (nonatomic, readonly) BOOL loadingMore;

@property (nonatomic, readonly) NSUInteger pageOffset;

- (id)initWithTableDisposer:(CTTableDisposerModeled *)aTableDisposer
          initialPageOffset:(NSUInteger)anInitialPageOffset
                itemsAsPage:(BOOL)anItemsAsPage;

- (void)setupMoreCellDataForSection:(CTSectionReadonly*)section withPagedModelsCount:(NSUInteger)modelsCount;

@end


@protocol CTModuleListPagingDelegate <CTModuleListDelegate>

- (CTCellData<CTPagingMoreCellDataProtocol>*)moreCellDataForPagingModule:(CTModuleListPaging*)aModule;

@end
