//
//  CTCompoundFetcher.h
//  CaigudaKit
//
//  Created by Malaar on 26.05.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTDataFetcher.h"

/**
 *
 * Use this fetcher to execute several other fetchers (this fetchers can be any type).
 * This fetchers can be executed parallel or consequentially.
 * You responsible to create one new Response for responses of other executed fetchers.
 *
 * Composit pattern used.
 **/
@interface CTCompoundFetcher : NSObject <CTDataFetcher>
{
    NSMutableArray* fetcherNodes;
    CTDataFetchCallback fetchCallback;
    
    CTFetcherMessage *originalMessage;
}

/**
 * Setup this parameter to YES if you want to execute all fetchers parallel.
 * Otherwise, if you want to execute all fetchers consequentially setup this parameter to NO.
 * NO, by default
 **/
@property (nonatomic, assign) BOOL executeFetchingParallel;

/**
 * NO, by default
 **/
@property (nonatomic, assign) BOOL successResponseIfAtLeastOne;

- (void)addFetcher:(id<CTDataFetcher>)aDataFetcher;

/**
 * Create one CTResponse from responses of all fetchers.
 * By default this method only merges boArrays and datadictionaries in new response.
 * Override it if you need your own rules to create new response.
 **/
- (CTResponse*)createFetcherResponse;

/**
 * When executing fetchers in serial mode you can override this method to determine when next node can be executed or 
 * executing should be finished. By default returns YES.
 **/
- (BOOL)canExecuteNextNodeAtIndex:(NSUInteger)anIndex withMessage:(CTFetcherMessage*)aMessage;

/**
 * When executing fetchers you can override this method to send properly configured message of original(input) message for node at index.
 * by default this method returns original message.
 * NOTE: you should create new configured message by configurating copy of original message - use [originalMessage copy]
 **/
- (CTFetcherMessage*)configuredMessageForNodeAtIndex:(NSUInteger)anIndex;

/**
 * returns YES by default
 **/
- (BOOL)shouldIncludeResponseResultsFromNodeAtIndex:(NSUInteger)anIndex;

@end

/**
 * This is inner class used in CTCompoundFetcher
 **/
@interface CTCompoundFetcherNode : NSObject

@property (nonatomic, strong) id<CTDataFetcher> fetcher;
@property (nonatomic, strong) CTResponse* response;

@end

