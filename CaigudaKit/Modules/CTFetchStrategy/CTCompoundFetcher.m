//
//  CTCompoundFetcher.m
//  CaigudaKit
//
//  Created by Malaar on 26.05.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTCompoundFetcher.h"

@interface CTCompoundFetcher ()

@property (nonatomic, strong) NSMutableArray* fetcherNodes;
@property (nonatomic, strong) CTDataFetchCallback fetchCallback;

- (BOOL)isAllFetchersComplete;
- (void)executeNodeAtIndex:(NSUInteger)anIndex withMessage:(CTFetcherMessage*)aMessage;

@end

@implementation CTCompoundFetcher

@synthesize fetcherNodes, fetchCallback, callbackQueue;

#pragma mark - Init

- (id)init
{
    self = [super init];
    if(self)
    {
        fetcherNodes = [NSMutableArray new];
    }
    return self;
}

- (void)dealloc
{
    self.callbackQueue = NULL;
}

#if CT_NEEDS_DISPATCH_RETAIN_RELEASE
- (void)setCallbackQueue:(dispatch_queue_t)aCallbackQueue
{
    if (callbackQueue != aCallbackQueue)
    {
        if (callbackQueue)
        {
            dispatch_release(callbackQueue);
            callbackQueue = NULL;
        }
        
        if (aCallbackQueue)
        {
            dispatch_retain(aCallbackQueue);
            callbackQueue = aCallbackQueue;
        }
    }
}
#endif

#pragma mark - Fetchers

- (void)addFetcher:(id<CTDataFetcher>)aDataFetcher
{
    NSAssert(self.callbackQueue, @"CTCompoundFetcher: callbackQueue is nil! Setup callbackQueue before add any fetcher.");

    CTCompoundFetcherNode* node = [CTCompoundFetcherNode new];
    node.fetcher = aDataFetcher;
    node.fetcher.callbackQueue = self.callbackQueue;
    [fetcherNodes addObject:node];
}

- (BOOL)isAllFetchersComplete
{
    BOOL result = YES;
    for(CTCompoundFetcherNode* node in fetcherNodes)
    {
        if(!node.response)
        {
            result = NO;
            break;
        }
    }
    return result;
}

#pragma mark - Create response

- (CTResponse*)createFetcherResponse
{
    CTResponse* response = [CTResponse new];

    NSMutableArray* models = [NSMutableArray array];
    BOOL success = (self.successResponseIfAtLeastOne) ? NO : YES;

    NSUInteger index = 0;
    for(CTCompoundFetcherNode* node in fetcherNodes)
    {
        if (node.response)
        {
            if ([self shouldIncludeResponseResultsFromNodeAtIndex:index])
            {
                [response.dataDictionary addEntriesFromDictionary:node.response.dataDictionary];
                [models addObjectsFromArray:node.response.boArray];
            }
            
            if (self.successResponseIfAtLeastOne)
                success |= node.response.success;
            else
                success &= node.response.success;
        }
        
        index++;
    }
    
    response.success = success;
    response.boArray = models;
    
    return response;
}

- (BOOL)shouldIncludeResponseResultsFromNodeAtIndex:(NSUInteger)anIndex
{
    return YES;
}

#pragma mark - Execute

- (BOOL)canExecuteNextNodeAtIndex:(NSUInteger)anIndex withMessage:(CTFetcherMessage*)aMessage
{
    return YES;
}

- (void)executeNodeAtIndex:(NSUInteger)anIndex withMessage:(CTFetcherMessage*)aMessage
{
    if (anIndex >= [fetcherNodes count])
    {
        CTResponse* response = [self createFetcherResponse];
        response.success = NO;
        self.fetchCallback(response);
        return;
    }
    
    __weak CTCompoundFetcher* __self = self;
    
    CTCompoundFetcherNode* node = [fetcherNodes objectAtIndex:anIndex];
    __weak CTCompoundFetcherNode* __node = node;
    [node.fetcher fetchDataByMessage:aMessage withCallback:^(CTResponse *aResponse)
     {
         __node.response = aResponse;
         
         if (anIndex + 1 < __self.fetcherNodes.count)
         {
            CTFetcherMessage *configuredMessage = [self configuredMessageForNodeAtIndex:anIndex + 1];
            if ([__self canExecuteNextNodeAtIndex:anIndex + 1 withMessage:configuredMessage])
            {
                [__self executeNodeAtIndex:anIndex + 1 withMessage:configuredMessage];
                return;
            }
         }
         
         if (__self.fetchCallback)
            __self.fetchCallback([__self createFetcherResponse]);
     }];
}

- (CTFetcherMessage*)configuredMessageForNodeAtIndex:(NSUInteger)anIndex
{
    return originalMessage;
}

#pragma mark - CTDataFetcher

- (BOOL)canFetchWithMessage:(CTFetcherMessage *)aMessage
{
    BOOL result = YES;
    for(CTCompoundFetcherNode* node in fetcherNodes)
    {
        result &= [node.fetcher canFetchWithMessage:aMessage];
    }
    return result;
}

- (void)fetchDataByMessage:(CTFetcherMessage*)aMessage
              withCallback:(CTDataFetchCallback)aFetchCallback
{
    fetchCallback = aFetchCallback;
    originalMessage = aMessage;
    
    __weak CTCompoundFetcher* __self = self;

    if(self.executeFetchingParallel)
    {
        // execute fetching parallel
        CTFetcherMessage *configuredMessage;
        NSUInteger index = 0;
        for (CTCompoundFetcherNode* node in fetcherNodes)
        {
            __weak CTCompoundFetcherNode* __node = node;
            configuredMessage = [self configuredMessageForNodeAtIndex:index];
            [node.fetcher fetchDataByMessage:configuredMessage withCallback:^(CTResponse *aResponse)
             {
                 __node.response = aResponse;
                 if ([__self isAllFetchersComplete])
                 {
                     if (__self.fetchCallback)
                         __self.fetchCallback([__self createFetcherResponse]);
                 }
             }];
            
            index++;
        }
    }
    else
    {
        // execute fetching consequentially
        CTFetcherMessage *configuredMessage = [self configuredMessageForNodeAtIndex:0];
        [self executeNodeAtIndex:0 withMessage:configuredMessage];
    }

}

- (void)cancelFetching
{
    for (CTCompoundFetcherNode* node in fetcherNodes)
        [node.fetcher cancelFetching];    
}

@end

@implementation CTCompoundFetcherNode

@end
