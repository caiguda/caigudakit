//
//  CTFetcherIntoStorage.h
//  CaigudaKit
//
//  Created by Malaar on 29.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTFetcherWithRequest.h"
#import "CTGatewayRequest.h"
#import "CTDataBaseRequest.h"
#import "CTStorage.h"

/**
 * This fetcher automaticaly save received data from server.
 * When fether start it check if interneat connection reachable. 
 * If YES - fetcher will fetch data from server. 
 * When data was fetched, fetcher will save this data into database (insert or update)
 * If NO - fetcher will fetch data from database.
 * WARNING: Setup callbackQueue before setup request.
 **/
@interface CTFetcherIntoStorage : CTFetcherWithRequest

@property (nonatomic, readonly) CTStorage* storage;

@property (nonatomic, assign) BOOL clearStorageBeforeSave;
@property (nonatomic, assign) BOOL fetchOnlyFromDataBase;
@property (nonatomic, assign) BOOL fetchFromDataBaseWhenGatewayRequestFailed;
@property (nonatomic, assign) BOOL fetchedModelsContainsDifferentEntities;
@property (nonatomic, assign) BOOL dontSaveIntoStorageAfterFetchFromGateway;
@property (nonatomic, strong) CTFetcherMessage* currentMessage;


- (id)initWithStorage:(CTStorage*)aStorage;

- (CTGatewayRequest*)gatewayRequestByMessage:(CTFetcherMessage*)aMessage;
- (CTDataBaseRequest*)dataBaseRequestByMessage:(CTFetcherMessage*)aMessage;

/**
 * You can here change array of received models.
 **/
- (NSMutableArray*)processFetchedModelsInResponse:(CTResponse*)aResponse;

- (void)willSaveIntoStorage:(NSArray*)aModels;
- (void)didSaveIntoStorage:(NSArray*)aModels;

- (NSSet*)attributesToUpdateInModel;

- (BOOL)canFetchFromDatabaseForFailedResponse:(CTResponse*)aResponse;

@end
