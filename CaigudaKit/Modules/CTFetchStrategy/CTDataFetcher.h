//
//  CTModuleFetchProtocol.h
//  CaigudaKit
//
//  Created by Malaar on 16.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTResponse.h"
#import "CTFetcherMessage.h"
#import "CTKitDefines.h"

typedef void (^CTDataFetchCallback) (CTResponse* aResponse);

/**
 * Always setup callbackQueue before use fetcher!
 **/
@protocol CTDataFetcher <NSObject>

#if !OS_OBJECT_USE_OBJC
    @property (nonatomic, assign) dispatch_queue_t callbackQueue;
#else
    @property (nonatomic, strong) dispatch_queue_t callbackQueue;
#endif

- (BOOL)canFetchWithMessage:(CTFetcherMessage*)aMessage;

- (void)fetchDataByMessage:(CTFetcherMessage*)aMessage
              withCallback:(CTDataFetchCallback)aFetchCallback;

- (void)cancelFetching;

@end
