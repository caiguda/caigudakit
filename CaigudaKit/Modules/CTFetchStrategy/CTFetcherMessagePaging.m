//
//  CTFetcherMessagePaging.m
//  CaigudaKit
//
//  Created by Malaar on 29.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTFetcherMessagePaging.h"

@implementation CTFetcherMessagePaging

- (NSString *)debugDescription
{
    return [NSString stringWithFormat:@"super - %@; pagingSize %lul ; pagingOffset %lul ; reloading : %d ; loadinMore : %d ;", [super debugDescription], (unsigned long)self.pagingSize, (unsigned long)self.pagingOffset, self.reloading, self.loadingMore];
}

#pragma mark - NSCopying

//- (id)copyWithZone:(NSZone *)zone
//{
//    CTFetcherMessagePaging* message = (CTFetcherMessagePaging*)[super copyWithZone:zone];
//    message.pagingSize = self.pagingSize;
//    message.pagingOffset = self.pagingOffset;
//    message.reloading = self.reloading;
//    message.loadingMore = self.loadingMore;
//    
//    return message;
//}

@end