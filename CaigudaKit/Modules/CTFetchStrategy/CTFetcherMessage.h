//
//  CTFetcherMessage.h
//  CaigudaKit
//
//  Created by Malaar on 20.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "SMModelObject.h"

@interface CTFetcherMessage : SMModelObject

@property (nonatomic, readonly) NSMutableDictionary *defaultParameters;

@end
