//
//  CTFetcherMessage.m
//  CaigudaKit
//
//  Created by Malaar on 20.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTFetcherMessage.h"

@implementation CTFetcherMessage

- (id)init
{
    self = [super init];
    if (self)
    {
        _defaultParameters = [NSMutableDictionary dictionary];
    }
    return self;
}

- (NSString *)debugDescription
{
    return [NSString stringWithFormat:@"defaultParams : %@", _defaultParameters];
}

#pragma mark - NSCopying

//- (id)copyWithZone:(NSZone *)zone
//{
//    CTFetcherMessage *message = [[self class] new];
//    [message.defaultParameters addEntriesFromDictionary:self.defaultParameters];
//    
//    return message;
//}

@end
