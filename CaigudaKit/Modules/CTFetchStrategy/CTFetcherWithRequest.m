//
//  CTDataFetchStrategy.m
//  CaigudaKit
//
//  Created by Malaar on 16.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTFetcherWithRequest.h"

@implementation CTFetcherWithRequest

@synthesize request;
@synthesize fetchCallback;
@synthesize callbackQueue;

- (void)dealloc
{
    self.callbackQueue = NULL;
}

#if CT_NEEDS_DISPATCH_RETAIN_RELEASE
- (void)setCallbackQueue:(dispatch_queue_t)aCallbackQueue
{
    if (callbackQueue != aCallbackQueue)
    {
        if (callbackQueue)
        {
            dispatch_release(callbackQueue);
            callbackQueue = NULL;
        }
        
        if (aCallbackQueue)
        {
            dispatch_retain(aCallbackQueue);
            callbackQueue = aCallbackQueue;
        }
    }
}
#endif

#pragma mark - Request

- (CTRequest*)preparedRequestByMessage:(CTFetcherMessage*)aMessage;
{
    NSAssert(NO, @"Override this method!");
    return nil;
}

- (void)setRequest:(CTRequest *)aRequest
{
    NSAssert(self.callbackQueue, @"CTFetcherWithRequest: callbackQueue is nil! Setup callbackQueue before setup request.");

    if(request != aRequest)
    {
        [self cancelFetching];
        request = aRequest;
        __weak CTFetcherWithRequest* __self = self;
        [request addResponseBlock:^(CTResponse *aResponse)
        {
            if (__self.fetchCallback)
                __self.fetchCallback(aResponse);
        } responseQueue:self.callbackQueue];
    }
}

- (BOOL)canFetchWithMessage:(CTFetcherMessage *)aMessage
{
    if(!preparedRequest)
        preparedRequest = [self preparedRequestByMessage:aMessage];
    
    return [preparedRequest canExecute];
}

- (void)fetchDataByMessage:(CTFetcherMessage*)aMessage
              withCallback:(CTDataFetchCallback)aFetchCallback
{
    fetchCallback = aFetchCallback;

    if(!preparedRequest)
        preparedRequest = [self preparedRequestByMessage:aMessage];
    
    self.request = preparedRequest;
    preparedRequest = nil;
    
    [request start];
}

- (void)cancelFetching
{
    [request cancel];
}

@end
