//
//  CTFetcherIntoStorage.m
//  CaigudaKit
//
//  Created by Malaar on 29.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTFetcherIntoStorage.h"
#import "CTStorableObject.h"
#import "CTGatewayConfigurator.h"

@interface CTFetcherIntoStorage ()


@end

@implementation CTFetcherIntoStorage

- (id)initWithStorage:(CTStorage*)aStorage
{
    NSParameterAssert(aStorage);
    self = [super init];
    if(self)
    {        
        _storage = aStorage;
    }
    return self;
}

#pragma mark - Request

- (void)setRequest:(CTRequest *)aRequest
{
    NSAssert(self.callbackQueue, @"CTFetcherWithRequest: callbackQueue is nil! Setup callbackQueue before setup request.");

    if(request == aRequest)
        return;
    
    [self cancelFetching];
    
    request = aRequest;
    
    __weak CTFetcherIntoStorage* __self = self;
    [request addResponseBlock:^(CTResponse *aResponse)
     {
         if([aRequest isKindOfClass:[CTGatewayRequest class]])
         {
             BOOL success = aResponse.success;
             
             if(success)
             {
                 if(aResponse.boArray.count)
                 {
                     NSMutableArray* models = [__self processFetchedModelsInResponse:aResponse];
                     aResponse.boArray = models;
                     
                     if(!__self.dontSaveIntoStorageAfterFetchFromGateway)
                     {
                         if(__self.clearStorageBeforeSave)
                             [__self.storage removeAllEntitiesAndInsertNewModels:models
                                                  arrayContainsDifferentEntities:__self.fetchedModelsContainsDifferentEntities];
                         else
                             [__self.storage insertModelsWithUpdate:models modelAttributesToUpdate:[__self attributesToUpdateInModel]];
                         
                         [__self willSaveIntoStorage:models];
                         [__self.storage save];
                         [__self didSaveIntoStorage:models];
                     }
                 }
                 
                 if (__self.fetchCallback)
                     __self.fetchCallback(aResponse);
             }
             else if(__self.fetchFromDataBaseWhenGatewayRequestFailed &&
                     !aResponse.requestCancelled &&
                     [__self canFetchFromDatabaseForFailedResponse:aResponse])
             {
                 __self.request = [__self dataBaseRequestByMessage:__self.currentMessage];
                 if (__self.request)    // if no request were created we should properly return and call fetchCallback
                     [__self.request start];
                 else if (__self.fetchCallback)
                     __self.fetchCallback(aResponse);
             }
             else
             {
                 if (__self.fetchCallback)
                     __self.fetchCallback(aResponse);
             }
         }
         else
         {
             if (__self.fetchCallback)
                 __self.fetchCallback(aResponse);
         }
         
     } responseQueue:self.callbackQueue];
}

- (CTRequest*)preparedRequestByMessage:(CTFetcherMessage*)aMessage
{
    if(self.currentMessage == aMessage)
        return preparedRequest;
    
    self.currentMessage = aMessage;
    
    CTRequest *newRequest;
    if(!self.fetchOnlyFromDataBase)
    {
        if([[CTGatewayConfigurator sharedInstance] isInternetReachable])
            newRequest = [self gatewayRequestByMessage:aMessage];
        else
            newRequest = [self dataBaseRequestByMessage:aMessage];
    }
    else
    {
        newRequest = [self dataBaseRequestByMessage:aMessage];
    }
    
    return newRequest;
}

- (CTGatewayRequest*)gatewayRequestByMessage:(CTFetcherMessage*)aMessage
{
    // override it
    return nil;
}

- (CTDataBaseRequest*)dataBaseRequestByMessage:(CTFetcherMessage*)aMessage
{
    // override it
    return nil;
}

#pragma mark - Fetch

- (void)fetchDataByMessage:(CTFetcherMessage*)aMessage
              withCallback:(CTDataFetchCallback)aFetchCallback
{
    fetchCallback = aFetchCallback;

    if(!preparedRequest)
        preparedRequest = [self preparedRequestByMessage:aMessage];
    
    self.request = preparedRequest;
    preparedRequest = nil;

    [request start];
}


- (NSMutableArray*)processFetchedModelsInResponse:(CTResponse *)aResponse
{
    return aResponse.boArray;
}

- (void)willSaveIntoStorage:(NSArray*)aModels
{
    // override it if you need
}

- (void)didSaveIntoStorage:(NSArray*)aModels
{
    // override it if you need
}

- (NSSet*)attributesToUpdateInModel
{
    // override it if you need
    return nil;
}

- (BOOL)canFetchFromDatabaseForFailedResponse:(CTResponse*)aResponse
{
    return YES;
}

@end
