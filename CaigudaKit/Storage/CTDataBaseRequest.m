//
//  CTDataBaseRequest.m
//  CaigudaKit
//
//  Created by Malaar on 16.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTDataBaseRequest.h"

@interface CTDataBaseRequest ()

- (CTResponse*)executeRequest:(NSFetchRequest*)aRequest
                    inContext:(NSManagedObjectContext*)aContext;

@end

@implementation CTDataBaseRequest

- (id)initWithStorage:(CTStorage*)aStorage
{
    self = [super init];
    if(self)
    {
        storage = aStorage;
    }
    return self;
}

#pragma mark - Request execute

- (BOOL)canExecute
{
    return storage && self.fetchRequest;
}

- (void)start
{
    cancelled = NO;
    executing = YES;
    __weak CTDataBaseRequest *weakself = self;
    
    [storage scheduleBlock:^
    {
        CTDataBaseRequest *strongself = weakself;
        if (strongself)
        {
            CTResponse* response = nil;
            if(strongself->cancelled)
            {
                strongself->executing = NO;
                
                response = [CTResponse new];
                response.requestCancelled = YES;
                response.success = NO;
                [strongself executeAllResponseBlocksWithResponse:response];
                return;
            }
            
            response = [strongself executeRequest:strongself.fetchRequest inContext:strongself->storage.managedObjectContext];
            
            strongself->executing = NO;
            if(self.executeAllResponseBlocksSync)
                [strongself executeSynchronouslyAllResponseBlocksWithResponse:response];
            else
                [strongself executeAllResponseBlocksWithResponse:response];
        }
    }];
}

- (void)execute
{
    cancelled = NO;
    executing = YES;
    __weak CTDataBaseRequest *weakself = self;
    __block CTResponse* response;
    
    [storage executeBlock:^
    {
        CTDataBaseRequest *strongself = weakself;
        if (strongself)
            response = [strongself executeRequest:strongself.fetchRequest inContext:strongself->storage.managedObjectContext];
    }];
        
    executing = NO;
    [self executeSynchronouslyAllResponseBlocksWithResponse:response];
}

#pragma mark -

- (CTResponse*)executeRequest:(NSFetchRequest*)aRequest inContext:(NSManagedObjectContext*)aContext
{
    NSError* error = nil;
    NSArray* results = [aContext executeFetchRequest:aRequest error:&error];
    
    CTResponse* response = [CTResponse new];
    if([results count])
        [response.boArray addObjectsFromArray:results];
    
    response.error = error;
    response.code = error.code;
    response.requestCancelled = [self isCancelled];
    response.success = !response.error && !response.requestCancelled;

    return response;
}

- (void)cancel
{
    cancelled = YES;
}

- (BOOL)isExecuting
{
    return executing;
}

- (BOOL)isCancelled
{
    return cancelled;
}

@end
