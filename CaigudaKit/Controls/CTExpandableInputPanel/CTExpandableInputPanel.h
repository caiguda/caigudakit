//
//  CTExpandableInputPanel.h
//  CaigudaKit
//
//  Created by Malaar on 18.06.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTTextView.h"
#import "CTTargetAction.h"

@protocol CTExpandableInputPanelDelegate;

@interface CTExpandableInputPanel : UIView <UITextViewDelegate, UIGestureRecognizerDelegate>
{
    CGFloat lineDiff;
    __weak UIView *overlayView;
    BOOL showOverlayView;
}

@property (nonatomic, weak) UIView* linkedView;
@property (nonatomic, weak) id<CTExpandableInputPanelDelegate> delegate;

@property (nonatomic, assign) NSUInteger maxLines;
@property (nonatomic, assign) CGFloat minHeight;

@property (nonatomic, assign) BOOL hideKeyboardOnTapOutside; // by default is YES
@property (nonatomic, assign) BOOL showOverlayView;         // by default is NO - show or not under panel dark transparent view

@property (nonatomic, strong) IBOutlet CTTextView *textView;
@property (nonatomic, strong) CTTargetAction *sendTargetAction; // default targetaction to use in subclasses for main action button


- (void)attachToOwnerView:(UIView*)anOwnerView
           withLinkedView:(UIView*)aLinkedView;
- (void)resizeOnOwnerView;
- (void)removeFromOwnerView;

- (void)insertText:(NSString*)text atCursorPositionWithWhiteSpaces:(BOOL)shouldInsertWhiteSpaces;
- (void)insertText:(NSString*)text atPosition:(NSUInteger)position withWhiteSpaces:(BOOL)shouldInsertWhiteSpaces;

- (void)clearText;

#pragma mark - Private
- (void)setup;

@end

@protocol CTExpandableInputPanelDelegate <UITextViewDelegate>

@optional
- (void)inputPanel:(CTExpandableInputPanel*)anInputPanel didChangeHeight:(float)aHeight;
- (void)linkedViewDidResizedForInputPanel:(CTExpandableInputPanel*)anInputPanel;

@end
