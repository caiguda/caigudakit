//
//  MUKeyboardToolbar.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 4/16/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTToolbar.h"

@protocol CTKeyboardToolbarProtocol <NSObject>

- (void)didNextButtonPressd;
- (void)didPrevButtonPressd;
- (void)didDoneButtonPressd;

@end

@interface CTKeyboardToolbar : CTToolbar
{
    UISegmentedControl *segmentedPreviousNext;
}

@property (nonatomic, retain) NSString *previousTitle;
@property (nonatomic, retain) NSString *nextTitle;
@property (nonatomic, weak) id<CTKeyboardToolbarProtocol> keyboardToolbarDelegate;
@property (nonatomic, readonly) UIBarButtonItem *doneButton;

- (void)selectedInputFieldIndex:(NSInteger)selectInsex allCountInputFields:(NSInteger)allCountInputFields;
- (void)hideSegmentedPreviousNext;

@end
