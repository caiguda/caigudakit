//
//  MUKeyboardAvoidingProtocol.h
//  CaigudaKit
//
//  Created by Malaar on 02.04.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTKeyboardToolbar.h"

@protocol CTKeyboardAvoiderProtocol;

@protocol CTKeyboardAvoidingProtocol <NSObject>

@property (nonatomic, readonly) CTKeyboardToolbar *keyboardToolbar;
@property (nonatomic, assign) BOOL showsKeyboardToolbar;

- (void)adjustOffset;
- (void)hideKeyBoard;

- (void)addObjectForKeyboard:(id<UITextInputTraits, CTKeyboardAvoiderProtocol>)objectForKeyboard;
- (void)removeObjectForKeyboard:(id<UITextInputTraits, CTKeyboardAvoiderProtocol>)objectForKeyboard;

- (void)addObjectsForKeyboard:(NSArray *)objectsForKeyboard;
- (void)removeObjectsForKeyboard:(NSArray *)objectsForKeyboard;

- (void)removeAllObjectsForKeyboard;

- (void)responderShouldReturn:(UIResponder*)aResponder;

@end
