//
//  MUKeyboardAvoiderProtocol.h
//  CaigudaKit
//
//  Created by Malaar on 02.04.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CTKeyboardAvoidingProtocol;

@protocol CTKeyboardAvoiderProtocol <NSObject>

@property (nonatomic, weak) id<CTKeyboardAvoidingProtocol> keyboardAvoiding;

@end
