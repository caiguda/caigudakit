//
//  MUKeyboardAvoidingScrollView.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 1/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//
//  Taken as a basis for an example of "TPKeyboardAvoidingScrollView"(Created by Michael Tyson)

#import <UIKit/UIKit.h>
#import "CTKeyboardAvoidingProtocol.h"
#import "CTKeyboardToolbar.h"

@interface CTKeyboardAvoidingScrollView : UIScrollView <CTKeyboardAvoidingProtocol, CTKeyboardToolbarProtocol>
{
    UIEdgeInsets    _priorInset;
    BOOL            _keyboardVisible;
    CGRect          _keyboardRect;
    CGSize          _originalContentSize;
    NSMutableArray *_objectsInKeyboard;
    
    NSUInteger _selectIndexInputField;    
    CTKeyboardToolbar *keyboardToolbar;
}

@end
