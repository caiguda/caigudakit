//
//  MUTextView.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 9/6/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTValidator.h"
#import "CTKeyboardAvoiderProtocol.h"
#import "CTFilter.h"

@class CTTextViewDelegateHolder;

@interface CTTextView : UITextView <UITextViewDelegate, CTValidationProtocol, CTKeyboardAvoiderProtocol>
{
    CTValidator* validator;
    CTTextViewDelegateHolder* delegateHolder;
    CTFilter *filter;
    
    NSString* placeholder;
    UIColor* placeholderColor;
    UIFont* placeholderFont;
}

@property (nonatomic, weak) id<UITextViewDelegate> ctdelegate;
@property (nonatomic, strong) CTFilter *filter;
@property (nonatomic, copy) NSString* observedText;

@property (nonatomic, retain) NSString* placeholder;
@property (nonatomic, retain) UIColor* placeholderColor;
@property (nonatomic, retain) UIFont* placeholderFont;

@end
