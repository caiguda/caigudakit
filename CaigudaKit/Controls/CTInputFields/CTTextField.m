//
//  MUTextField.m
//  CaigudaKit
//
//  Created by Malaar on 8/19/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTTextField.h"
#import "CTKeyboardAvoidingProtocol.h"


@interface CTTextFieldDelegateHolder : NSObject <UITextFieldDelegate>

@property (nonatomic, weak) CTTextField* holdedTextField;

@end

@interface CTTextField ()

- (void)setup;

@end

@implementation CTTextField

@synthesize validatableText;
@synthesize ctdelegate;
@synthesize keyboardAvoiding;
@synthesize filter;

#pragma mark - Init/Dealloc

- (id)init
{
    self = [super init];
    if (self)
    {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self setup];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if( (self = [super initWithCoder:aDecoder]) )
    {
        [self setup];
    }
    return self;
}

- (void)setup
{
    self.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    delegateHolder = [CTTextFieldDelegateHolder new];
    delegateHolder.holdedTextField = self;
    super.delegate = delegateHolder;
}

- (void)setPadding:(CGFloat)aPadding
{
    if (aPadding > 0)
    {
        UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, aPadding, self.bounds.size.height)];
        self.leftView = view;
        self.leftViewMode = UITextFieldViewModeAlways;
        
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, aPadding, self.bounds.size.height)];
        self.rightView = view;
        self.rightViewMode = UITextFieldViewModeAlways;
    }
    else
    {
        self.leftView = nil;
        self.leftViewMode = UITextFieldViewModeNever;
        self.rightView = nil;
        self.rightViewMode = UITextFieldViewModeNever;
    }
}

- (CGRect)caretRectForPosition:(UITextPosition *)position
{
    if (self.hidenCursor)
        return CGRectZero;
    else
        return [super caretRectForPosition:position];
}

#pragma mark - CTValidationProtocol

- (void)setValidator:(CTValidator*)aValidator
{
    validator = aValidator;
    validator.validatableObject = self;
}

- (NSString*)validatableText
{
    return self.text;
}

- (void)setValidatableText:(NSString *)aValidatableText
{
    self.text = aValidatableText;
}

- (CTValidator*)validator
{
    return validator;
}

- (BOOL)validate
{
    return (validator) ? ([validator validate]) : (YES);
}

#pragma mark - CTFormatterProtocol

- (void)setFormatter:(CTFormatter*)aFormatter
{
    formatter = aFormatter;
    formatter.formattableObject = self;
}

- (CTFormatter*)formatter
{
    return formatter;
}

- (NSString *)formattingText
{
    return self.text;
}

#pragma mark - UITextFieldDelegate

- (void)setDelegate:(id<UITextFieldDelegate>)aDelegate
{
    ctdelegate = aDelegate;
}

- (id<UITextFieldDelegate>)delegate
{
    return ctdelegate;
}

@end


@implementation CTTextFieldDelegateHolder

@synthesize holdedTextField;

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    BOOL result = YES;
    
    if([holdedTextField.ctdelegate respondsToSelector:@selector(textFieldShouldBeginEditing:)])
        result = [holdedTextField.ctdelegate textFieldShouldBeginEditing:textField];
    
    return result;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [holdedTextField.keyboardAvoiding adjustOffset];
    
    if([holdedTextField.ctdelegate respondsToSelector:@selector(textFieldDidBeginEditing:)])
        [holdedTextField.ctdelegate textFieldDidBeginEditing:textField];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    BOOL result = YES;
    
    if([holdedTextField.ctdelegate respondsToSelector:@selector(textFieldShouldEndEditing:)])
        result = [holdedTextField.ctdelegate textFieldShouldEndEditing:textField];
    
    return result;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if([holdedTextField.ctdelegate respondsToSelector:@selector(textFieldDidEndEditing:)])
        [holdedTextField.ctdelegate textFieldDidEndEditing:textField];
}

- (BOOL)textField:(CTTextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    BOOL result = YES;
    if (textField.filter)
        result = [textField.filter textInField:textField shouldChangeCharactersInRange:range replacementString:string];
    
    if(result && [textField formatter])
        result = [[textField formatter] formatWithNewCharactersInRange:range replacementString:string];

    // at this moment we should know latest version of textfield.text and
    // if formatter setuped he can format text by its own logic and returns result NO - even in this case we may want to make some stuff in delegate method
    if([holdedTextField.ctdelegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)])
        result &= [holdedTextField.ctdelegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
    
    return result;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    BOOL result = YES;
    
    if([holdedTextField.ctdelegate respondsToSelector:@selector(textFieldShouldClear:)])
        result = [holdedTextField.ctdelegate textFieldShouldClear:textField];
    
    return result;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    BOOL result = YES;
    
    if([holdedTextField.ctdelegate respondsToSelector:@selector(textFieldShouldReturn:)])
        result = [holdedTextField.ctdelegate textFieldShouldReturn:textField];
    
    if(result)
        [holdedTextField.keyboardAvoiding responderShouldReturn:textField];
    
    return result;
}

@end
