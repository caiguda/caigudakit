//
//  CTAlertView.m
//  CaigudaKitDemo
//
//  Created by Malaar on 04.09.14.
//  Copyright (c) 2014 Caiguda. All rights reserved.
//

#import "CTAlertView.h"

@implementation CTAlertView

- (id)init
{
    self = [super init];
    if(self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(hide) name:UIApplicationDidEnterBackgroundNotification object:nil];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)hide
{
    [self dismissWithClickedButtonIndex:-1 animated:YES];
}

@end
