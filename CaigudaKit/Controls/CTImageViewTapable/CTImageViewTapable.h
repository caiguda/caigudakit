//
//  MUImageViewTapable.h
//  CaigudaKitMaster
//
//  Created by Malaar on 3/14/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTTargetAction.h"


@interface CTImageViewTapable : UIImageView
{
    NSMutableArray* targetActions;
    NSObject* data;
}

@property (nonatomic, assign) BOOL enable;
@property (nonatomic, strong) NSObject* data;

- (void)addTarget:(id)aTarget action:(SEL)anAction;

@end
