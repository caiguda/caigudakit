//
//  MUImageViewTapable.m
//  CaigudaKit
//
//  Created by Malaar on 3/14/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTImageViewTapable.h"
//#import "UIImageView+AFNetworking.h"

@interface CTImageViewTapable ()

- (void)initialize;
- (void)handleTap:(UITapGestureRecognizer*)aTapRecognizer;

@end


@implementation CTImageViewTapable

@synthesize enable;
@synthesize data;

- (id)init
{
    if( (self = [super init]) )
    {
        [self initialize];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if( (self = [super initWithCoder:aDecoder]) )
    {
        [self initialize];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if( (self = [super initWithFrame:frame]) )
    {
        [self initialize];
    }
    return self;
}

- (id)initWithImage:(UIImage *)image
{
    self = [super initWithImage:image];
    if(self)
    {
        [self initialize];
    }
    return self;
}

- (id)initWithImage:(UIImage *)image highlightedImage:(UIImage *)highlightedImage
{
    self = [super initWithImage:image highlightedImage:highlightedImage];
    if(self)
    {
        [self initialize];
    }
    return self;
}

- (void)initialize
{
    enable = YES;
    targetActions = [NSMutableArray new];
    self.userInteractionEnabled = YES;
    
    UITapGestureRecognizer* tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    [self addGestureRecognizer:tapRecognizer];
}

- (void)addTarget:(id)aTarget action:(SEL)anAction
{
    CTTargetAction* ta = [[CTTargetAction alloc] initWithTarget:aTarget action:anAction];
    [targetActions addObject:ta];
}

- (void)handleTap:(UITapGestureRecognizer*)aTapRecognizer
{
    if(!enable)
        return;
    
    if(aTapRecognizer.state == UIGestureRecognizerStateEnded)
    {
        [targetActions makeObjectsPerformSelector:@selector(sendActionFrom:) withObject:self];
    }
}

@end
