//
//  MUTabBar.h
//  CaigudaKit
//
//  Created by Malaar on 7/25/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTToolbar.h"

@class CTTabedToolbar;

@protocol CTTabedToolbarDelegate <NSObject>

@optional
- (BOOL)tabedToolbar:(CTTabedToolbar*)aTabBar shouldSelectItemAtIndex:(NSUInteger)anIndex;
- (void)tabedToolbar:(CTTabedToolbar*)aTabBar itemChangedTo:(NSUInteger)aToIndex from:(NSUInteger)aFromIndex;

@end


@interface CTTabedToolbar : CTToolbar
{
    UIButton* currentItem;
    NSMutableArray* buttons;
	BOOL enabled;
}

@property (nonatomic, weak) id<CTTabedToolbarDelegate> tabedToolbarDelegate;
@property (nonatomic, readonly) NSArray* buttons;
@property (nonatomic, assign) BOOL enabled;

- (void)switchToItemWithIndex:(NSUInteger)aIndex;        ///< programmaticaly switch to title by index

@end
