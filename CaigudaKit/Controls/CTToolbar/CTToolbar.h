//
//  CTTabBar.h
//  CaigudaKit
//
//  Created by Malaar on 7/25/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTToolbar : UIToolbar

@property (nonatomic, retain) UIImage* backgroundImage;
@property (nonatomic, assign) BOOL drawsColor;

@end
