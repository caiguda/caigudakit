//
//  CTPopupCustomSimplePicker.h
//  Pods
//
//  Created by Malaar on 28.07.13.
//
//

#import "CTPopupSimplePicker.h"
#import "CTPopupPickerItemTitled.h"

@interface CTPopupCustomSimplePicker : CTPopupSimplePicker

@property (nonatomic, weak) UIFont* font;
@property (nonatomic, strong) UIColor* textColor;
@property (nonatomic, assign) NSTextAlignment textAlignment;
@property (nonatomic, strong) UIColor* shadowColor;
@property (nonatomic, assign) CGSize shadowOffset;

@end
