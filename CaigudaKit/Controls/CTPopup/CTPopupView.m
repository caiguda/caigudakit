//
//  MUPopupView.m
//  CaigudaKit
//
//  Created by Malaar on 2/13/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTPopupView.h"
#import "CTPopoverViewController.h"
#import "CTKitDefines.h"


@implementation CTPopupView

@synthesize showStrategy;
@synthesize hideByTapOutside;
@synthesize showOverlayView;
@synthesize isVisible;

#pragma mark - Init/Dealloc

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if( (self = [super initWithCoder:aDecoder]) )
    {
        [self setup];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    if( (self = [super initWithFrame:frame]) )
    {
        [self setup];
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void) setup
{
    hideByTapOutside = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(notificationNeedHide:) name:CT_POPUPVIEW_NEED_HIDE object:nil];

}

- (void)prepareToRelease
{
    if([showStrategy isKindOfClass:[CTPopupViewController class]])
    {
        showStrategy = nil;
    }
}

- (void)prepareToShow
{
    showStrategy = nil;
    
    if(!CT_IS_IPAD)
    {
        CTPopupViewController* popupController = [[CTPopupViewController alloc] init];
        popupController.popupedView = self;
        
        showStrategy = popupController;
    }
    else
    {
        CTPopoverViewController* popoverViewController = [[CTPopoverViewController alloc] initWithPopupView:self];
        UIPopoverController* popoverController = [[UIPopoverController alloc] initWithContentViewController:popoverViewController];
        popoverViewController.popoverOwner = popoverController;
        popoverViewController = nil;
        
        showStrategy = popoverController;
    }
}

- (void)popupWillAppear:(BOOL)animated
{
    isVisible = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:CT_POPUPVIEW_WILL_SHOW object:self];
}

- (void)popupDidAppear:(BOOL)animated
{
	[[NSNotificationCenter defaultCenter] postNotificationName:CT_POPUPVIEW_DID_SHOW object:self];
}

- (void)popupWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] postNotificationName:CT_POPUPVIEW_WILL_HIDE object:self];
}

- (void)popupDidDisappear:(BOOL)animated
{
    showStrategy = nil;

    isVisible = NO;
	[[NSNotificationCenter defaultCenter] postNotificationName:CT_POPUPVIEW_DID_HIDE object:self];
}

- (void)hideWithAnimation:(BOOL)animation
{
    if(!CT_IS_IPAD)
    {
        [(CTPopupViewController*)showStrategy hideWithAnimation:animation];
    }
    else
    {
        [(UIPopoverController*)showStrategy dismissPopoverAnimated:animation];
    }
    
}

- (void)showWithAnimation:(BOOL)animation inView:(UIView*)aView
{
    if(!CT_IS_IPAD)
    {
        [(CTPopupViewController*)showStrategy showWithAnimation:animation inView:aView];
    }
}

- (void)showFromRect:(CGRect)rect inView:(UIView *)view permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections animated:(BOOL)animated
{
    if(CT_IS_IPAD)
    {
        [(UIPopoverController*)showStrategy presentPopoverFromRect:rect inView:view permittedArrowDirections:arrowDirections animated:animated];
    }
}

#pragma mark - NSNotificationNeedHide
- (void)notificationNeedHide:(NSNotification *)aNotification
{
    [self hideWithAnimation:YES];
}
@end
