//
//  CTPopupPickerItemTitled.h
//  CaigudaKit
//
//  Created by Malaar on 19.10.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol CTPopupPickerItemTitled <NSObject>

@property (nonatomic, readonly) NSString* itemTitle;

@end
