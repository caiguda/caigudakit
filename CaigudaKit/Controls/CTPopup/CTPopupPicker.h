//
//  MUPopupPicker.h
//  CaigudaKit
//
//  Created by Malaar on 9/23/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTPopupView.h"
#import "CTToolbar.h"

#define CT_POPUPVIEW_TOOLBAR_ITEM_DID_PRESSED      @"CTPopupViewToolbarItemDidPressed"
#define CT_POPUPVIEW_TOOLBAR_ITEM_PRESSED_INDEX    @"CTPopupViewToolbarItemPressedIndex"

@interface CTPopupPicker : CTPopupView
{
    CTToolbar* toolbar;
    UIView* picker;
    
    id selectedItem;
}

@property (nonatomic, retain) CTToolbar* toolbar;
@property (nonatomic, retain) id selectedItem;

- (UIView*)createPicker;

@end
