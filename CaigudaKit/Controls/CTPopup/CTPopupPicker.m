//
//  MUPopupPicker.m
//  CaigudaKit
//
//  Created by Malaar on 9/23/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTPopupPicker.h"

@interface CTPopupPicker ()

- (void)toolbarButtonPressed:(UIButton*)aSender;
- (void)toolbarItemPressed:(UIBarButtonItem*)aSender;
- (void)configureFrames;

@end

@implementation CTPopupPicker

@synthesize toolbar;
@synthesize selectedItem;

- (void)dealloc
{
    if ([picker isKindOfClass:[UIPickerView class]])
    {
        ((UIPickerView*)picker).delegate = nil;
        ((UIPickerView*)picker).dataSource = nil;
    }
}

// Create, configure and return popupedView
- (void)setup
{
    [super setup];
    
    self.frame = CGRectMake(0, 0, 320, 216);
    
    self.backgroundColor = [UIColor clearColor];
    picker = [self createPicker];
    [self addSubview:picker];
    
    [self configureFrames];
}

- (void)configureFrames
{
    self.autoresizingMask ^= !(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin);
    picker.autoresizingMask ^= !(UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
    
    CGRect frame = picker.frame;
    frame.origin.y = toolbar.bounds.size.height;
    picker.frame = frame;
    
    frame.origin.y = 0;
    frame.size.height += toolbar.bounds.size.height;
    self.frame = frame;
    
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    picker.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
}

- (void)setToolbar:(CTToolbar *)aToolbar
{
    if(toolbar == aToolbar)
        return;
    
    if(aToolbar)
    {
        toolbar = aToolbar;
        [self addSubview:toolbar];
        [toolbar sizeToFit];
        toolbar.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        
        [self configureFrames];

        int index = 0;
        for(UIBarButtonItem* bbi in toolbar.items)
        {
            if(bbi.customView && [bbi.customView isKindOfClass:[UIButton class]])
            {
                bbi.customView.tag = index;
                [((UIButton*)bbi.customView) addTarget:self action:@selector(toolbarButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
            }
            else
            {
                bbi.tag = index;
                [bbi setTarget:self];
                [bbi setAction:@selector(toolbarItemPressed:)];
            }
            
            ++index;
        }
    }
    else
    {
        [toolbar removeFromSuperview];
        toolbar = nil;
    }
}

- (UIView*)createPicker
{
    // override it in subclasses
    return nil;
}

- (id)selectedItem
{
    // override it in subclasses
    return nil;
}

#pragma mark - Toolbar

- (void)toolbarButtonPressed:(UIButton*)aSender
{
    NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
    [userInfo setObject:@(aSender.tag) forKey:CT_POPUPVIEW_TOOLBAR_ITEM_PRESSED_INDEX];
    [[NSNotificationCenter defaultCenter] postNotificationName:CT_POPUPVIEW_TOOLBAR_ITEM_DID_PRESSED object:self userInfo:userInfo];
}

- (void)toolbarItemPressed:(UIBarButtonItem*)aSender
{
    NSMutableDictionary* userInfo = [NSMutableDictionary dictionary];
    [userInfo setObject:@(aSender.tag) forKey:CT_POPUPVIEW_TOOLBAR_ITEM_PRESSED_INDEX];
    [[NSNotificationCenter defaultCenter] postNotificationName:CT_POPUPVIEW_TOOLBAR_ITEM_DID_PRESSED object:self userInfo:userInfo];
}

@end
