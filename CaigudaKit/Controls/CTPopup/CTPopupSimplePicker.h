//
//  MUPopupPicker.h
//  CaigudaKit
//
//  Created by Malaar on 8/11/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTPopupPicker.h"
#import "CTPopupPickerItemTitled.h"

#define CT_POPUPPICKER_VALUE_DID_CHANGE    @"CT_POPUPPICKER_VALUE_DID_CHANGE"

/**
 * This class can work with dataSource elements of classes MUTitledID or NSString, else generate assert.
 * This picker has only one component in pickerView.
 */
@interface CTPopupSimplePicker : CTPopupPicker <UIPickerViewDelegate, UIPickerViewDataSource>
{
    NSArray* dataSource;
}

@property (nonatomic, copy) NSArray* dataSource;
@property (nonatomic, readonly) UIPickerView* popupedPicker;

@end
