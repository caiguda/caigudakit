//
//  MUSearchBar.h
//  CaigudaKit
//
//  Created by Malaar on 7/19/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTValidator.h"
#import "CTFilter.h"
#import "CTFormatter.h"

@class CTSearchBarDelegateHolder;

@interface CTSearchBar : UISearchBar <UISearchBarDelegate, CTValidationProtocol, CTFormatterProtocol>
{
    CTValidator* validator;
    CTFormatter* formatter;
    CTSearchBarDelegateHolder* delegateHolder;
}

@property (nonatomic, weak) id<UISearchBarDelegate> ctdelegate;
@property (nonatomic, retain) CTFilter *filter;

@end
