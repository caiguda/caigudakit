//
//  AutoresizeButton.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 6/30/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef struct
{
    int topCapHeight;
    int leftCapHeight;
} CTImageCapSize;

static inline CTImageCapSize CTImageCapSizeMake(int leftCapHeight, int topCapHeight)
{
    CTImageCapSize imageCapSize;
    imageCapSize.leftCapHeight = leftCapHeight;
    imageCapSize.topCapHeight = topCapHeight;
    return imageCapSize;
}

static inline CTImageCapSize CTImageCapSizeNone()
{
    CTImageCapSize imageCapSize;
    imageCapSize.leftCapHeight = -1;
    imageCapSize.topCapHeight = -1;
    return imageCapSize;
}

typedef struct
{
    int leftOffset;
    int rightOffset;
} CTTitleOffset;

static inline CTTitleOffset CTTitleOffsetMake(int leftOffset, int rightOffset)
{
    CTTitleOffset titleOffset;
    titleOffset.leftOffset = leftOffset;
    titleOffset.rightOffset = rightOffset;
    return titleOffset;
}

static inline CTTitleOffset CTTitleOffsetZero()
{
    CTTitleOffset titleOffset;
    titleOffset.leftOffset = 0;
    titleOffset.rightOffset = 0;
    return titleOffset;
}

static inline CTTitleOffset CTTitleOffsetDefault()
{
    CTTitleOffset titleOffset;
    titleOffset.leftOffset = 9;
    titleOffset.rightOffset = 9;
    return titleOffset;
}

static inline CTTitleOffset CTTitleOffsetForNavBack()
{
    CTTitleOffset titleOffset;
    titleOffset.leftOffset = 15;
    titleOffset.rightOffset = 9;
    return titleOffset;
}

/// Autoresized button
@interface CTAutoresizeButton : UIButton 
{
    NSString *title;
    UIImage* originalBGImage;
    CTImageCapSize imageCapSize;
    CTTitleOffset titleOffset;
    int maxWidth;
    int minWidth;
}

@property (nonatomic, assign) int maxWidth;
@property (nonatomic, assign) int minWidth;

+(id)buttonByImageName:(NSString*)imageName;

+(id)buttonByTitleName:(NSString*)titleName
             imageName:(NSString*)imageName;

+(id)buttonByTitleName:(NSString*)titleName
             imageName:(NSString*)imageName
          imageCapSize:(CTImageCapSize)imageCapSize;

+(id)buttonByTitleName:(NSString*)titleName
             imageName:(NSString*)imageName
           titleOffset:(CTTitleOffset)titleOffset;

+(id)buttonByTitleName:(NSString*)titleName
             imageName:(NSString*)imageName
          imageCapSize:(CTImageCapSize)imageCapSize
           titleOffset:(CTTitleOffset)titleOffset;

+(id)buttonForNavBackByTitleName:(NSString*)titleName
                       imageName:(NSString*)imageName;

- (void)setupFont:(UIFont *)aFont;
- (void)setAutoresizeTitle:(NSString*)aTitle;

@end
