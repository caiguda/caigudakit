//
//  CTPullToRefreshOD.m
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTPullToRefreshOD.h"

@interface CTPullToRefreshOD ()

- (void)refreshControlDidBeginRefreshing:(ODRefreshControl*)aSender;

@end

@implementation CTPullToRefreshOD

@synthesize refreshControl;

- (void)configureWithTableView:(UITableView*)aTableView
{
    refreshControl = [[ODRefreshControl alloc] initInScrollView:aTableView];
    [refreshControl addTarget:self action:@selector(refreshControlDidBeginRefreshing:) forControlEvents:UIControlEventValueChanged];
}

- (void)releaseRefreshControl
{
    refreshControl = nil;
}

- (void)beginPullToRefresh
{
    [refreshControl beginRefreshing];
    [super beginPullToRefresh];
}

- (void)endPullToRefresh
{
    [refreshControl endRefreshing];
}

- (void)refreshControlDidBeginRefreshing:(ODRefreshControl*)aSender
{
    if(refreshControl != aSender)
        return;

    [self beginPullToRefresh];
}

@end
