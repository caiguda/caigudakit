//
//  CTPullToRefreshOD.h
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTPullToRefreshAdapter.h"
#import <ODRefreshControl/ODRefreshControl.h>

/**
 * This adapter provide wrapper around ODRefreshControl.
 * @see CTPullToRefreshAdapter
 */
@interface CTPullToRefreshOD : CTPullToRefreshAdapter

@property (nonatomic, readonly) ODRefreshControl* refreshControl;

@end
