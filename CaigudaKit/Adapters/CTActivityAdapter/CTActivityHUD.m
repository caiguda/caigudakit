//
//  CTActivityHUD.m
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTActivityHUD.h"

@implementation CTActivityHUD

- (id)activity
{
    return activity;
}

- (void)configureWithView:(UIView*)aView
{
    activity = [[MBProgressHUD alloc] initWithView:aView];
    [aView addSubview:activity];
}

- (void)showActivity:(BOOL)animated
{
    [activity.superview bringSubviewToFront:activity];
    [activity show:animated];
}

- (void)hideActivity:(BOOL)animated
{
    [activity hide:animated];
}

- (void)releaseActivityView
{
    [activity removeFromSuperview];
    activity = nil;
}

@end
