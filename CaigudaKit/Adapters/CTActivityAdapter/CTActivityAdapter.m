//
//  CTActivityAdapter.m
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTActivityAdapter.h"

@implementation CTActivityAdapter

@dynamic activity;

- (id)activity
{
    // override it in subclasses
    return nil;
}

- (void)configureWithView:(UIView*)aView
{
    // override it in subclasses
}

- (void)showActivity:(BOOL)animated
{
    // override it in subclasses
}

- (void)hideActivity:(BOOL)animated
{
    // override it in subclasses
}

- (void)releaseActivityView
{
    // override it in subclasses
}

@end
