//
//  CTCompoundCell.h
//
//
//  Created by Malaar on 11.02.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTCell.h"
#import "CTCellDataModeled.h"
#import "CTCompoundModel.h"

@class CTTableDisposerModeled;

@interface CTCompoundCell : CTCell
{
    NSMutableArray* subCells;
    NSMutableArray* verticalSeparatorLines;
    
    // for dequeue cells
    NSMutableDictionary *reusableCells;
}

- (UIView*)verticalSeparatorLineAtIndex:(NSUInteger)index;

@end


@interface CTCompoundCellData : CTCellDataModeled

@property (nonatomic, weak) CTTableDisposerModeled* tableDisposer;
@property (nonatomic, readonly) NSMutableArray* cellDatas;

/*
 * spacing between cells
 * by default eq 0
 */
@property (nonatomic, assign) NSUInteger itemSpacing;

/*
 * insets for rectangle which cells will occupy inside CompoundCell
 * UIEdgeInsetsZero by default
 */
@property (nonatomic, assign) UIEdgeInsets itemInsets;

@property (nonatomic, strong) UIColor* verticalSeparatorLinesColor;
@property (nonatomic, assign) BOOL showsVerticalSeparatorLines;     // by default is NO

- (CGFloat)subCellWidthForCompoundCellWidth:(CGFloat)aWidth;

@end