//
//  CTTableDisposerMaped.h
//  CaigudaKit
//
//  Created by Malaar on 30.03.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTTableDisposer.h"

@interface CTTableDisposerMapped : CTTableDisposer

- (void)mapToObject;

@end
