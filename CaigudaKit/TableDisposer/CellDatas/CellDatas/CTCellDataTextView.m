//
//  MUCellDataTextView.m
//  CaigudaKit
//
//  Created by Malaar on 04.04.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataTextView.h"
#import "CTCellTextView.h"


@implementation CTCellDataTextView

@synthesize textAlignment;
@synthesize autocapitalizationType;
@synthesize autocorrectionType;
@synthesize keyboardType;
@synthesize keyboardAppearance;
@synthesize returnKeyType;

@synthesize validator;
@synthesize filter;

@synthesize placeholder;
@synthesize placeholderColor;

#pragma mark - Init/Dealloc

- (id)initWithObject:(NSObject *)aObject key:(NSString *)aKey
{
    self = [super initWithObject:aObject key:aKey];
    if(self)
    {
        self.cellClass = [CTCellTextView class];
        self.cellSelectionStyle = UITableViewCellSelectionStyleNone;
        
        textAlignment = NSTextAlignmentLeft;
        autocapitalizationType = UITextAutocapitalizationTypeNone;
        autocorrectionType = UITextAutocorrectionTypeNo;
        keyboardType = UIKeyboardTypeDefault;
        keyboardAppearance = UIKeyboardAppearanceDefault;
        returnKeyType = UIReturnKeyDefault;
        
        self.cellHeight = 90;
    }
    return self;
}

@end
