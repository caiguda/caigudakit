//
//  MUTextCellData.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataLabel.h"
#import "CTCellLabel.h"

@implementation CTCellDataLabel

#pragma mark - Init/Dealloc

- (id)initWithObject:(NSObject *)aObject key:(NSString *)aKey
{
    self = [super initWithObject:aObject key:aKey];
    if(self)
    {
        self.cellClass = [CTCellLabel class];
    }
    return self;
}

@end
