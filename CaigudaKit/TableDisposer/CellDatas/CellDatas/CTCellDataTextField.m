//
//  MUEntryCellData.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataTextField.h"
#import "CTCellTextField.h"


@implementation CTCellDataTextField

@synthesize textSecured;
@synthesize placeholder, placeholderColor;

@synthesize autocapitalizationType;
@synthesize autocorrectionType;
@synthesize keyboardType;
@synthesize keyboardAppearance;
@synthesize returnKeyType;

@synthesize validator;
@synthesize filter;

#pragma mark - Init/Dealloc

- (id)initWithObject:(NSObject *)aObject key:(NSString *)aKey
{
    self = [super initWithObject:aObject key:aKey];
    if(self)
    {
        self.cellClass = [CTCellTextField class];
        self.cellSelectionStyle = UITableViewCellSelectionStyleNone;

        autocapitalizationType = UITextAutocapitalizationTypeNone;
        autocorrectionType = UITextAutocorrectionTypeNo;
        keyboardType = UIKeyboardTypeDefault;
        keyboardAppearance = UIKeyboardAppearanceDefault;
        returnKeyType = UIReturnKeyDefault;
    }
    return self;
}

@end
