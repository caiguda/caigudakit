//
//  MUButtonCellData.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataButton.h"
#import "CTCellButton.h"

@implementation CTCellDataButton

@synthesize targetAction;

@synthesize title;
@synthesize titleFont;
@synthesize titleColor;
@synthesize titleAlignment;

#pragma mark - Init/Dealloc

- (id)init
{
    self = [super init];
    if(self)
    {
        self.cellClass = [CTCellButton class];
        
        titleAlignment = NSTextAlignmentLeft;
        titleFont = [UIFont systemFontOfSize:18];
        titleColor = [UIColor blackColor];
        targetAction = [[CTTargetAction alloc] init];
    }
    return self;
}

#pragma mark - Target/Action

- (void)setTarget:(id)aTarget action:(SEL)anAction
{
    [targetAction setTarget:aTarget action:anAction];
    [self addCellSelectedTarget:targetAction.target action:targetAction.action];
}

@end
