//
//  MUBooleanCellData.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataStandart.h"
#import "CTTargetAction.h"


@interface CTCellDataSwitch : CTCellDataStandart

@property (nonatomic, assign) BOOL boolValue;
@property (nonatomic, readonly) CTTargetAction* targetAction;

@property (nonatomic, retain) NSString* onText;
@property (nonatomic, retain) NSString* offText;

- (void)setTarget:(id)aTarget action:(SEL)anAction;

@end
