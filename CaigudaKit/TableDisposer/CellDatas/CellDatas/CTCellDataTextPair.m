//
//  MUCellDataTextPair.m
//  CaigudaKit
//
//  Created by Malaar on 04.04.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataTextPair.h"

@implementation CTCellDataTextPair

@synthesize title, titleFont, titleColor;
@synthesize text, textFont, textColor;

#pragma mark - Init/Dealloc

- (id)initWithObject:(NSObject *)aObject key:(NSString *)aKey
{
    self = [super initWithObject:aObject key:aKey];
    if(self)
    {
        titleFont = [UIFont systemFontOfSize:16];
        titleColor = [UIColor blackColor];
        
        textFont = [UIFont systemFontOfSize:16];
        textColor = [UIColor blackColor];
    }
    return self;
}

#pragma mark - Maping

- (void)mapFromObject
{
    if (object && key)
        text = [object valueForKeyPath:key];
}

- (void)mapToObject
{
    if (object && key)
        [object setValue:text forKeyPath:key];
}

@end
