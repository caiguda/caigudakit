//
//  MUButtonCellData.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellData.h"
#import "CTTargetAction.h"

@interface CTCellDataButton : CTCellData

@property (nonatomic, retain) NSString* title;
@property (nonatomic, retain) UIColor *titleColor;
@property (nonatomic, strong) UIFont *titleFont;
@property (nonatomic, assign) NSTextAlignment titleAlignment;

@property (nonatomic, readonly) CTTargetAction* targetAction;

- (void)setTarget:(id)aTarget action:(SEL)anAction;

@end
