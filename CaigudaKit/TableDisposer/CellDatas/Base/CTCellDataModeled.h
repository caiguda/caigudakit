//
//  MUCellDataModeled.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/29/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellData.h"

@interface CTCellDataModeled : CTCellData

@property (nonatomic, readonly) id model;

- (id)initWithModel:(id)aModel;

@end
