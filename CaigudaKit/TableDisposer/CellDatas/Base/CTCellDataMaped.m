//
//  MUCellDataMaped.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/29/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellDataMaped.h"

@implementation CTCellDataMaped

@synthesize key;
@synthesize object;

- (id)init
{
    NSAssert(NO, @"You can't use this method! Instead use 'initWithObject:key:'");
    return nil;
}

- (id)initWithObject:(NSObject *)aObject key:(NSString *)aKey
{
    self = [super init];
    if (self)
    {
        key = aKey;
        object = aObject;
    }
    return self;
}

- (void) mapFromObject
{
    NSAssert(nil, @"Override this method in subclasses!");
}

- (void) mapToObject
{
    NSAssert(nil, @"Override this method in subclasses!");
}

@end
