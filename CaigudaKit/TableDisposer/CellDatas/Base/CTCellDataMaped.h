//
//  MUCellDataMaped.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/29/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellData.h"

@interface CTCellDataMaped : CTCellData
{
    NSString *key;
    NSObject *object;
}

@property (nonatomic, readonly) NSString *key;
@property (nonatomic, readonly) NSObject *object;

- (id)initWithObject:(NSObject *)aObject key:(NSString *)aKey;

- (void)mapFromObject;
- (void)mapToObject;

@end
