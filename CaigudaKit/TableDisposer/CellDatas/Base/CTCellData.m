//
//  MUCellData.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/29/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellData.h"
#import "CTTargetAction.h"

@implementation CTCellData

@synthesize cellNibName;
@synthesize cellClass;
@dynamic cellIdentifier;
@synthesize cellSelectionStyle;
@synthesize cellSeparatorStyle;
@synthesize cellStyle;
@synthesize cellAccessoryType;
@synthesize autoDeselect;
@synthesize visible;
@synthesize enableEdit;
@synthesize disableInputTraits;
@synthesize cellHeight,cellWidth;
@synthesize tag;
@synthesize userData;

- (id)init
{
    self = [super init];
    if (self)
    {
        visible = YES;
        autoDeselect = YES;
        enableEdit = YES;
        cellSelectionStyle = UITableViewCellSelectionStyleBlue;
        cellStyle = UITableViewCellStyleDefault;
        cellAccessoryType = UITableViewCellAccessoryNone;
        
        cellSelectedHandlers = [NSMutableArray new];
        cellDeselectedHandler = [NSMutableArray new];
        
        cellHeight = 44.f;
        cellWidth = 0.0f;
        cellSeparatorStyle = CTCellSeparatorStyleNone;
    }
    return self;
}

- (CGFloat)cellHeightForWidth:(CGFloat) aWidth
{
    return cellHeight;
}

- (NSString *)cellIdentifier
{
    return NSStringFromClass([self class]);
}

#pragma mark - Handlers

- (void)addCellSelectedTarget:(id)aTarget action:(SEL)anAction
{
    [cellSelectedHandlers addObject:[CTTargetAction targetActionWithTarget:aTarget action:anAction]];
}

- (void)addCellDeselectedTarget:(id)aTarget action:(SEL)anAction
{
    [cellDeselectedHandler addObject:[CTTargetAction targetActionWithTarget:aTarget action:anAction]];
}

- (void)performSelectedHandlers
{
    for(CTTargetAction* handler in cellSelectedHandlers)
    {
        [handler sendActionFrom:self];
    }
}

- (void)performDeselectedHandlers
{
    for(CTTargetAction* handler in cellDeselectedHandler)
    {
        [handler sendActionFrom:self];
    }
}

#pragma mark - Create cell

- (CTCell*)createCell
{
    CTCell* cell = nil;
    
    if(cellNibName)
        cell = (CTCell*)[[[NSBundle mainBundle] loadNibNamed:cellNibName owner:self options:nil] lastObject];
    else
        cell = [[self.cellClass alloc] initWithStyle:self.cellStyle reuseIdentifier:self.cellIdentifier];
    
    return cell;
}

@end
