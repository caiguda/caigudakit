//
//  MUCellProtocol.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/29/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#ifndef CaigudaKit_MUCellProtocol_h
#define CaigudaKit_MUCellProtocol_h

#import "CTCellData.h"

@protocol CTCellProtocol <NSObject>

@property (nonatomic, readonly) CTCellData *cellData;

- (void)setupCellData:(CTCellData *)aCellData;

@end

#endif
