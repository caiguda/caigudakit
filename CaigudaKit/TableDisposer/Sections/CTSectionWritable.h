//
//  MUSectionWritable.h
//  CaigudaKit
//
//  Created by Malaar on 29.03.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTSectionReadonly.h"

@interface CTSectionWritable : CTSectionReadonly
{
    NSMutableArray* cells;
}

- (void)createCells;

- (void)mapFromObject;
- (void)mapToObject;

@end
