//
//  MUSectionReadonly.h
//  CaigudaKit
//
//  Created by Malaar on 29.03.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTCell.h"
#import "CTCellData.h"

@class CTTableDisposer;

@interface CTSectionReadonly : NSObject
{
    NSMutableArray* cellDataSource;
    NSMutableArray* visibleCellDataSource;
    __weak CTTableDisposer* disposer;
}

@property (nonatomic, retain) NSString* headerTitle;
@property (nonatomic, retain) NSString* footerTitle;
@property (nonatomic, retain) UIView* headerView;
@property (nonatomic, retain) UIView* footerView;

#pragma mark - Init/Dealloc

+ (instancetype)section;

- (void)setTableDisposer:(CTTableDisposer*)aTableDisposer;

- (void)addCellData:(CTCellData*)aCellData;
- (void)addCellDataFromArray:(NSArray*)aCellDataArray;
- (void)insertCellData:(CTCellData*)aCellData atIndex:(NSUInteger)anIndex;
- (void)removeCellDataAtIndex:(NSUInteger)anIndex;
- (void)removeAllCellData;
- (NSUInteger)cellDataCount;
- (NSUInteger)visibleCellDataCount;

- (CTCellData*)cellDataAtIndex:(NSUInteger)anIndex;
- (CTCellData*)visibleCellDataAtIndex:(NSUInteger)anIndex;
- (NSUInteger)indexByCellData:(CTCellData*)aCellData;
- (NSUInteger)indexByVisibleCellData:(CTCellData*)aCellData;
- (CTCellData*)cellDataByTag:(NSUInteger)aTag;

- (void)updateCellDataVisibility;

- (CTCell*)cellForIndex:(NSUInteger)anIndex;

- (void)reloadWithAnimation:(UITableViewRowAnimation)anAnimation;
- (void)reloadRowsAtIndexes:(NSArray*)anIndexes withAnimation:(UITableViewRowAnimation)aRowAnimation;
- (void)deleteRowsAtIndexes:(NSArray*)anIndexes withAnimation:(UITableViewRowAnimation)aRowAnimation;

- (void)showCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)showCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable withAnimation:(UITableViewRowAnimation)aRowAnimation;
- (void)hideCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable;
- (void)hideCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable withAnimation:(UITableViewRowAnimation)aRowAnimation;

@end
