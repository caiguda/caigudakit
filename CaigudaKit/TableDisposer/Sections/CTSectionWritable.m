//
//  MUSectionWritable.m
//  CaigudaKit
//
//  Created by Malaar on 29.03.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTSectionWritable.h"
#import "CTCellDataMaped.h"
#import "CTCellData.h"
#import "CTCellProtocol.h"
#import "CTTableDisposer.h"
#import "CTKeyboardAvoidingTableView.h"
#import "CTKeyboardAvoidingProtocol.h"

@interface CTSectionWritable ()

- (CTCell*)createCellAtIndex:(NSUInteger)anIndex;

@end


@implementation CTSectionWritable

#pragma mark - Init/Dealloc

- (id)init
{
    if( (self = [super init]) )
    {
        cells = [NSMutableArray new];
    }
    return self;
}

#pragma mark - Cells

- (void)createCells
{
    // remove old cells
    [cells removeAllObjects];
    
    [self updateCellDataVisibility];

    CTCell* cell;
    for (NSUInteger index = 0; index < visibleCellDataSource.count; ++index)
    {
        cell = [self createCellAtIndex:index];
        [cells addObject:cell];
    }
}

- (UITableViewCell<CTCellProtocol>*)cellForIndex:(NSUInteger)anIndex
{
    UITableViewCell<CTCellProtocol>* cell = [cells objectAtIndex:anIndex];
    // ...
    return cell;
}

- (void)reloadWithAnimation:(UITableViewRowAnimation)anAnimation
{
    if ([disposer.tableView conformsToProtocol:@protocol(CTKeyboardAvoidingProtocol)])
    {
        for (CTCell *cell in cells)
        {
            [(id<CTKeyboardAvoidingProtocol>)disposer.tableView removeObjectsForKeyboard:[cell inputTraits]];
        }
    }

    [self mapFromObject];
    
    [super reloadWithAnimation:anAnimation];
}

- (void)reloadRowsAtIndexes:(NSArray *)anIndexes withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    NSMutableArray* indexPaths = [NSMutableArray array];
    NSIndexPath* indexPath;
    NSInteger sectionIndex = [disposer indexBySection:self];
    
    CTCellData* cellData;
    CTCell* cell;
    for(NSNumber* index in anIndexes)
    {
        cellData = [self visibleCellDataAtIndex:[index integerValue]];
        if([cellData isKindOfClass:[CTCellDataMaped class]])
        {
            [(CTCellDataMaped*)cellData mapFromObject];
        }
        
        cell = [self cellForIndex:[index integerValue]];
        [cell setupCellData:cellData];

        indexPath = [NSIndexPath indexPathForRow:[index integerValue] inSection:sectionIndex];
        [indexPaths addObject:indexPath];
    }
    
    [disposer.tableView reloadRowsAtIndexPaths:indexPaths withRowAnimation:aRowAnimation];
}


#pragma mark - Show/Hide cels

- (void)hideCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    CTCellData* cellData = [self cellDataAtIndex:anIndex];
    if(!cellData.visible)
        return;
    
    NSUInteger index = [self indexByVisibleCellData:cellData];
    
    CTCell* cell = [self cellForIndex:index];
    if ([disposer.tableView conformsToProtocol:@protocol(CTKeyboardAvoidingProtocol)])
    {
        [((id<CTKeyboardAvoidingProtocol>)disposer.tableView) removeObjectsForKeyboard:[cell inputTraits]];
    }
    
    [visibleCellDataSource removeObjectAtIndex:index];
    [cells removeObjectAtIndex:index];
    cellData.visible = NO;
    
    if(aNeedUpdateTable)
    {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:[disposer indexBySection:self]];
        [disposer.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:aRowAnimation];
    }
}

- (void)showCellByIndex:(NSUInteger)anIndex needUpdateTable:(BOOL)aNeedUpdateTable withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    CTCellData* cellData = [self cellDataAtIndex:anIndex];
    if(cellData.visible)
        return;

    cellData.visible = YES;
    [self updateCellDataVisibility];
    
    NSUInteger index = [self indexByVisibleCellData:cellData];
    CTCell* cell = [self createCellAtIndex:index];
    [cells insertObject:cell atIndex:index];

    if(aNeedUpdateTable)
    {
        NSIndexPath* indexPath = [NSIndexPath indexPathForRow:index inSection:[disposer indexBySection:self]];
        [disposer.tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:aRowAnimation];
    }
}

#pragma mark - Maping

- (void)mapFromObject
{
    for (CTCellData* cellData in cellDataSource)
    {
        if ([cellData isKindOfClass:[CTCellDataMaped class]])
            [(CTCellDataMaped*)cellData mapFromObject];
    }
    [self createCells];
}

- (void)mapToObject
{
    for (CTCellData* cellData in cellDataSource)
    {
        if ([cellData isKindOfClass:[CTCellDataMaped class]])
            [(CTCellDataMaped*)cellData mapToObject];
    }
}

- (void)deleteRowsAtIndexes:(NSArray*)anIndexes withAnimation:(UITableViewRowAnimation)aRowAnimation
{
    [super deleteRowsAtIndexes:anIndexes withAnimation:aRowAnimation];
    
    NSMutableIndexSet* set = [NSMutableIndexSet new];
    for(NSNumber* index in anIndexes)
    {
        [set addIndex:[index unsignedIntegerValue]];
    }
    [cells removeObjectsAtIndexes:set];
}

#pragma mark - Private

- (CTCell*)createCellAtIndex:(NSUInteger)anIndex
{
    CTCellData* cellData = [self visibleCellDataAtIndex:anIndex];
    CTCell* cell = [cellData createCell];
    [cell setupCellData:cellData];
    
    if ([disposer.tableView conformsToProtocol:@protocol(CTKeyboardAvoidingProtocol)])
    {
        [((id<CTKeyboardAvoidingProtocol>)disposer.tableView) addObjectsForKeyboard:[cell inputTraits]];
        [((CTKeyboardAvoidingTableView*)disposer.tableView) sordetInputTraits:[cell inputTraits] byIndexPath:[NSIndexPath indexPathForRow:anIndex inSection:[disposer indexBySection:self]]];
    }
    
    [disposer didCreateCell:cell];
    
    return cell;
}

@end
