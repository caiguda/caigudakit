//
//  CTCell.h
//  CaigudaKit
//
//  Created by Malaar on 30.03.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTCellProtocol.h"

#define kCTCellDefaultSeparatorColor [UIColor colorWithRedI:224 greenI:224 blueI:224 alphaI:255]
#define kCTCellSeparatorHeight 1.0f

@interface CTCell : UITableViewCell <CTCellProtocol>

- (NSArray*)inputTraits;

@end
