//
//  CTCell.m
//  CaigudaKit
//
//  Created by Malaar on 30.03.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCell.h"
#import "CTCellData.h"
#import <QuartzCore/CALayer.h>
#import "CTKitDefines.h"

#define kCTCellTopSeparatorTag    113344
#define kCTCellBottomSeparatorTag 223344

@implementation CTCell

@synthesize cellData;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self)
    {
        if(CT_IS_IOS7)
            self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    if(CT_IS_IOS7)
        self.backgroundColor = [UIColor clearColor];
}

#pragma mark - Setup

- (void)setupCellData:(CTCellData *)aCellData
{
    if (cellData != aCellData)
    {
        cellData = aCellData;
    }
    
    if (aCellData.cellWidth > 0)
    {
        CGRect frame = self.frame;
        frame.size.width = cellData.cellWidth;
        self.frame = frame;
        if (!self.superview)
        {
            self.contentView.frame = self.bounds;
        }
    }
    
    self.selectionStyle = cellData.cellSelectionStyle;
    self.accessoryType = cellData.cellAccessoryType;
    self.tag = cellData.tag;
    
    [self configureSeparators];
}

- (NSArray*)inputTraits
{
    return nil;
}

- (NSString*)reuseIdentifier
{
    return self.cellData.cellIdentifier;
}

#pragma mark - Separators

- (void)configureSeparators
{
    UIView *topSeparator = [self viewWithTag:kCTCellTopSeparatorTag];    
    if ((self.cellData.cellSeparatorStyle & CTCellSeparatorStyleTop) && self.cellData.cellTopSeparatorColor)
    {
        if (!topSeparator)
        {
            topSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.bounds.size.width, kCTCellSeparatorHeight)];
            topSeparator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin;
            topSeparator.layer.borderWidth = kCTCellSeparatorHeight;
            topSeparator.tag = kCTCellTopSeparatorTag;
            [self addSubview:topSeparator];
        }
        
        topSeparator.layer.borderColor = self.cellData.cellTopSeparatorColor.CGColor;
        [self bringSubviewToFront:topSeparator];
    }
    else
        [topSeparator removeFromSuperview];
    
    UIView *bottomSeparator = [self viewWithTag:kCTCellBottomSeparatorTag];
    if ((self.cellData.cellSeparatorStyle & CTCellSeparatorStyleBottom) && self.cellData.cellBottomSeparatorColor)
    {
        if (!bottomSeparator)
        {
            bottomSeparator = [[UIView alloc] initWithFrame:CGRectMake(0, self.bounds.size.height - kCTCellSeparatorHeight, self.bounds.size.width, kCTCellSeparatorHeight)];
            bottomSeparator.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
            bottomSeparator.layer.borderWidth = kCTCellSeparatorHeight;
            bottomSeparator.tag = kCTCellBottomSeparatorTag;
            [self addSubview:bottomSeparator];
        }
        
        bottomSeparator.layer.borderColor = self.cellData.cellBottomSeparatorColor.CGColor;
        [self bringSubviewToFront:bottomSeparator];
    }
    else
        [bottomSeparator removeFromSuperview];
}

#pragma mark - Layout

- (void)layoutSubviews
{
    [super layoutSubviews];
    if (!CT_IS_IOS8)
    {
        CGRect frame = self.contentView.frame;
        frame.size.height = self.frame.size.height;
        if ((self.cellData.cellSeparatorStyle & CTCellSeparatorStyleTop) && self.cellData.cellTopSeparatorColor)
        {
            frame.origin.y += kCTCellSeparatorHeight;
            frame.size.height -= kCTCellSeparatorHeight;
        }
        
        if ((self.cellData.cellSeparatorStyle & CTCellSeparatorStyleBottom) && self.cellData.cellBottomSeparatorColor)
        {
            frame.size.height -= kCTCellSeparatorHeight;
        }
        
        self.contentView.frame = frame;
    }
}

@end
