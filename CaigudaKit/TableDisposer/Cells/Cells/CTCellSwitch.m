//
//  MUBooleanCell.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellSwitch.h"
#import "CTCellDataSwitch.h"
#import "DCRoundSwitch.h"


@interface CTCellSwitch ()

- (void)didChangeBoolValueInSwitch:(UISwitch *) aSwitch;

@end

@implementation CTCellSwitch

- (void)setupCellData:(CTCellDataSwitch *)aCellData
{
    [super setupCellData:aCellData];
    
    id switcher = nil;
    
    if (aCellData.onText && aCellData.offText) 
    {
        switcher = [DCRoundSwitch new];
        ((DCRoundSwitch*)switcher).onText = aCellData.onText;
        ((DCRoundSwitch*)switcher).offText = aCellData.offText;
        ((DCRoundSwitch*)switcher).on = aCellData.boolValue;
        ((DCRoundSwitch*)switcher).enabled = aCellData.enableEdit;
    }
    else
    {
        switcher = [UISwitch new];
        ((UISwitch*)switcher).on = aCellData.boolValue;
        ((UISwitch*)switcher).enabled = aCellData.enableEdit;
        [switcher sizeToFit];
    }
    
    [switcher addTarget:self action:@selector(didChangeBoolValueInSwitch:) forControlEvents:UIControlEventValueChanged];
    
    self.accessoryView = switcher;
}

#pragma mark - Change Bool Value

- (void)didChangeBoolValueInSwitch:(UISwitch *) aSwitch
{
    ((CTCellDataSwitch*)self.cellData).boolValue = aSwitch.on;

    CTCellDataSwitch* cellData = (CTCellDataSwitch*)self.cellData;
    [cellData.targetAction sendActionFrom:cellData];
}

@end
