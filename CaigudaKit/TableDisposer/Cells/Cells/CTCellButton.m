//
//  MUButtonCell.m
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCellButton.h"
#import "CTCellDataButton.h"

@implementation CTCellButton

- (void)setupCellData:(CTCellDataButton *)aCellData
{
    [super setupCellData:aCellData];
    
    self.textLabel.text = aCellData.title;
    self.textLabel.textAlignment = aCellData.titleAlignment;
    self.textLabel.textColor = aCellData.titleColor;
    self.textLabel.font = aCellData.titleFont;
}

@end
