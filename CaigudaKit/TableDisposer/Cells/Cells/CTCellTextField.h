//
//  MUEntryCell.h
//  CaigudaKit
//
//  Created by Yuriy Bosov on 3/30/12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import "CTCell.h"
#import "CTTextField.h"

@interface CTCellTextField : CTCell
{
    UILabel *titleLabel;
}

@property (nonatomic, readonly ) CTTextField* textField;

@end
