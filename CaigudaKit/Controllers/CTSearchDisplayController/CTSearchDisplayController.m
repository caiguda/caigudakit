//
//  CTSearchDisplayController.m
//
//
//  Created by Alexander Burkhai on 8/21/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTSearchDisplayController.h"
#import "CTKitDefines.h"

@implementation CTSearchDisplayController

- (id)initWithSearchBar:(UISearchBar *)searchBar contentsController:(UIViewController *)viewController
{
    self = [super initWithSearchBar:searchBar contentsController:viewController];
    if (self)
    {
        self.shouldHideNavigationBar = YES;
    }
    return self;
}

- (void)setActive:(BOOL)visible animated:(BOOL)animated
{
    if (self.isActive == visible)
        return;
    
    if (CTSystemVersionIsAbove6x)
    {
        [super setActive:visible animated:animated];
    }
    else
    {
        if (!self.shouldHideNavigationBar)
        {
            [self.searchContentsController.navigationController setNavigationBarHidden:YES animated:NO];
            [super setActive:visible animated:animated];
            [self.searchContentsController.navigationController setNavigationBarHidden:NO animated:NO];
        }
        else
            [super setActive:visible animated:animated];
        
        if (visible)
            [self.searchBar becomeFirstResponder];
        else
            [self.searchBar resignFirstResponder];
    }
}

@end
