//
//  CTBaseViewController.m
//  CaigudaKit
//
//  Created by Malaar on 7/7/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTBaseViewController.h"
#import "UIAlertView+BlocksKit.h"
#import "CTActivityHUD.h"
#import "CTAlertView.h"

@implementation CTBaseViewController

@synthesize isVisible;

#pragma mark - View lifecycle

- (void)loadView
{
    [super loadView];

    activity = [CTActivityHUD new];
    [activity configureWithView:self.view];

    // bg image
    UIImage* bgImage = [self backgroundImage];
    if (bgImage)
    {
        CGRect frame = self.view.bounds;
        _backgroundImageView = [[UIImageView alloc] initWithFrame:frame];
        _backgroundImageView.autoresizingMask = CTViewAutoresizingFlexibleSize;
        [_backgroundImageView setImage:bgImage];
        [self.view addSubview:_backgroundImageView];
        [self.view sendSubviewToBack:_backgroundImageView];
    }

    // left nav button
    if(!self.navigationItem.hidesBackButton)
    {
        // custom left button
        UIBarButtonItem* leftNavigationButton = [self createLeftNavButton];
        if(leftNavigationButton)
        {
            if ([leftNavigationButton.customView isKindOfClass:[UIButton class]])
            {
                [((UIButton*)leftNavigationButton.customView) addTarget:self
                                                                 action:@selector(leftNavButtonPressed:)
                                                       forControlEvents:UIControlEventTouchUpInside];
            }
            else
            {
                leftNavigationButton.target = self;
                leftNavigationButton.action = @selector(leftNavButtonPressed:);
            }
            self.navigationItem.leftBarButtonItem = leftNavigationButton;
        }
    }

    // right nav button
    UIBarButtonItem* rightNavigationButton = [self createRightNavButton];
    if(rightNavigationButton)
    {
        if ([rightNavigationButton.customView isKindOfClass:[UIButton class]])
        {
            [((UIButton*)rightNavigationButton.customView) addTarget:self
                                                              action:@selector(rightNavButtonPressed:)
                                                    forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            rightNavigationButton.target = self;
            rightNavigationButton.action = @selector(rightNavButtonPressed:);
        }
        self.navigationItem.rightBarButtonItem = rightNavigationButton;
    }
    
    // custom title view for nav.item
    UIView *titleView = [self createTitleViewNavItem];
    if (titleView)
    {
        self.navigationItem.titleView = titleView;
    }
}

- (void)viewDidUnload
{
    [activity releaseActivityView];
    [super viewDidUnload];
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
    isVisible = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    isVisible = NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait || interfaceOrientation == UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - Activity

- (void)showActivity
{
    [activity showActivity:YES];
}

- (void)hideActivity;
{
    [activity hideActivity:YES];
}

#pragma mark - Show Alert View

- (void)showAlertViewWithTitle:(NSString*)aTitle message:(NSString*)aMessage
{
    if (isVisible)
    {
        CTShowSimpleAlert(aTitle, aMessage);
    }
}

- (void)showAlertViewWithTitle:(NSString *)aTitle
                       message:(NSString *)aMessage
             cancelButtonTitle:(NSString *)aCancelButtonTitle
             otherButtonTitles:(NSArray *)aOtherButtonTitles
                  dismissBlock:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))dismissBlock
{
    if (isVisible)
    {
//        [UIAlertView showAlertViewWithTitle:aTitle
//                                    message:aMessage
//                          cancelButtonTitle:aCancelButtonTitle
//                          otherButtonTitles:aOtherButtonTitles
//                                    handler:dismissBlock];
        
        [CTAlertView bk_showAlertViewWithTitle:aTitle
                                       message:aMessage
                             cancelButtonTitle:aCancelButtonTitle
                             otherButtonTitles:aOtherButtonTitles
                                       handler:dismissBlock];
    }
}

#pragma mark - Customization For Navigation Bar

// create custom left button for navigation bar
- (UIBarButtonItem*)createLeftNavButton
{
    return nil;
}

// create custom right button for navigation bar
- (UIBarButtonItem*)createRightNavButton
{
    return nil;
}

// create custom title view for navigation bar (return nil by default)
- (UIView*)createTitleViewNavItem
{
    return nil;
}

#pragma mark - Process Events

// action to process pressed-on-left-button event
- (void)leftNavButtonPressed:(id)aSender
{
    [self.navigationController popViewControllerAnimated:YES];
}

// action to process pressed-on-right-button event
- (void)rightNavButtonPressed:(id)aSender
{
    // empty by default
}

#pragma mark - Backgroung Image

- (UIImage*)backgroundImage
{
    return nil;
}

@end
