//
//  CTBaseViewController.h
//  CaigudaKit
//
//  Created by Malaar on 7/7/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CTKitDefines.h"
#import "CTExtensions.h"
#import "CTHelper.h"
#import "CTActivityAdapter.h"

/**
 * This class provide useful methods that simplify work with subclasses of UIViewController.
 * Good practice to create base view controller for all view controllers in your project. Useful to inherit this base controller from this class.
 */
@interface CTBaseViewController : UIViewController
{
    BOOL isVisible;
    CTActivityAdapter* activity;
}

/**
 * This property helps to determine is controller visible now.
 */
@property (nonatomic, readonly) BOOL isVisible;

/**
 * Gat access to view with background image of your controller.
 */
@property (nonatomic, readonly) UIImageView *backgroundImageView;

/**
 * Show activity. As activity used wrapper around activity control.
 * @see CTActivityAdapter
 * As example, you can show activity during loading some data, during send request to the server, or when executing some continuous task.
 * @see hideActivity
 */
- (void)showActivity;

/**
 * Hide activity.
 * @see showActivity
 */
- (void)hideActivity;

/**
 * Setup background image for controller view with this method. Just overrid it in your subclass.
 */
- (UIImage*)backgroundImage;

/**
 * Create custom left button for navigation bar
 */
- (UIBarButtonItem*)createLeftNavButton;

/**
 * Create custom title view for navigation bar (return nil by default)
 */
- (UIView*)createTitleViewNavItem;

/**
 * Create custom right button for navigation bar (return nil by default).
 */
- (UIBarButtonItem*)createRightNavButton;

#pragma mark - Actions

/**
 * Action to process pressed-on-left-button event
 */
- (void)leftNavButtonPressed:(id)aSender;

/**
 * Action to process pressed-on-right-button event
 */
- (void)rightNavButtonPressed:(id)aSender;

/**
 * Handy  method to call alert. Also this method before show alert checks is this controller visible.
 */
- (void)showAlertViewWithTitle:(NSString*)aTitle
                       message:(NSString*)aMessage;

/**
 * Handy  method to call alert. Also this method before show alert checks is this controller visible.
 */
- (void)showAlertViewWithTitle:(NSString*)aTitle
                       message:(NSString*)aMessage
             cancelButtonTitle:(NSString*)aCancelButtonTitle
             otherButtonTitles:(NSArray*)aOtherButtonTitles
                  dismissBlock:(void(^)(UIAlertView *alertView, NSInteger buttonIndex))dismissBlock;

@end
