//
//  CTResponse.m
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTResponse.h"

@implementation CTResponse

- (id)init
{
    self = [super init];
    if(self)
    {
        _dataDictionary = [NSMutableDictionary new];
        _boArray = [NSMutableArray new];
    }
    return self;
}

@end
