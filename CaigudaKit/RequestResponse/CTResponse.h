//
//  CTResponse.h
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTResponse : NSObject

@property (nonatomic, assign) BOOL success;

@property (nonatomic, assign) NSInteger code;
@property (nonatomic, strong) NSString* textMessage;

@property (nonatomic, readonly) NSMutableDictionary* dataDictionary;
@property (nonatomic, strong) NSMutableArray* boArray;

@property (nonatomic, strong) NSError *error;
@property (nonatomic, assign, getter = isRequestCancelled) BOOL requestCancelled;

@end
