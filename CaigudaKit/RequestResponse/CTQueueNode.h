//
//  CTQueueNode.h
//  CaigudaKit
//
//  Created by Alexander Burkhai on 8/6/13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTKitDefines.h"

@interface CTQueueNode : NSObject
{
    void* queueTag;
}

@property (nonatomic, assign) void* queueTag;

#if CT_NEEDS_DISPATCH_RETAIN_RELEASE
    @property (nonatomic, assign) dispatch_queue_t dispatchQueue;
#else
    @property (nonatomic, strong) dispatch_queue_t dispatchQueue;
#endif

@end
