//
//  CTRequest.m
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTRequest.h"
#import "CTQueueNode.h"

@interface CTResponseNode : CTQueueNode

@property (nonatomic, copy) CTRequestResponseBlock block;

@end

@implementation CTRequest

- (id)init
{
    self = [super init];
    if(self)
    {
        responseBlocks = [NSMutableArray new];
    }
    return self;
}

#pragma mark - Request execute

- (BOOL)canExecute
{
    return NO;
}

- (void)start
{
    NSAssert(NO, @"Override it!");
}

- (void)cancel
{
    NSAssert(NO, @"Override it!");
}

- (BOOL)isExecuting
{
    NSAssert(NO, @"Override it!");
    return NO;
}

- (BOOL)isCancelled
{
    NSAssert(NO, @"Override it!");
    return NO;
}

- (BOOL)isFinished
{
    NSAssert(NO, @"Override it!");
    return YES;
}

#pragma mark - Response blocks

- (void)addResponseBlock:(CTRequestResponseBlock)aResponseBlock responseQueue:(dispatch_queue_t)aResponseQueue
{
    NSParameterAssert(aResponseBlock);
    NSParameterAssert(aResponseQueue);
    
    CTResponseNode* node = [CTResponseNode new];
    node.block = aResponseBlock;
    node.dispatchQueue = aResponseQueue;
    
    [responseBlocks addObject:node];
}

- (void)clearAllResponseBlocks
{
    [responseBlocks removeAllObjects];
}

- (void)executeAllResponseBlocksWithResponse:(CTResponse*)aResponse
{
    for(CTResponseNode* node in responseBlocks)
    {
        if (node.dispatchQueue && node.block)
        {
            dispatch_async(node.dispatchQueue, ^
            {
                node.block(aResponse);
            });
        }
    }
}

- (void)executeSynchronouslyAllResponseBlocksWithResponse:(CTResponse*)aResponse
{
//    dispatch_queue_t currentQueue = dispatch_get_current_queue();
    for(CTResponseNode* node in responseBlocks)
    {
        if (!node.dispatchQueue || !node.block)
            continue;
        
//        if(currentQueue != node.dispatchQueue)
        if(dispatch_get_specific(node.queueTag))
        {
            node.block(aResponse);
//            dispatch_sync(node.dispatchQueue, ^
//            {
//                node.block(aResponse);
//            });
        }
        else
        {
            dispatch_sync(node.dispatchQueue, ^
            {
                node.block(aResponse);
            });
//            node.block(aResponse);
        }
    }
}

@end

@implementation CTResponseNode

@end

