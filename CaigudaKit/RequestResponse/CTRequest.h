//
//  CTRequest.h
//  CaigudaKit
//
//  Created by Malaar on 15.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTKitDefines.h"
#import "CTResponse.h"

typedef void (^CTRequestResponseBlock)(CTResponse* aResponse);

@interface CTRequest : NSObject
{
    NSMutableArray* responseBlocks;
}

@property (nonatomic, assign) BOOL executeAllResponseBlocksSync;

#pragma mark - Request execute/cancel
- (BOOL)canExecute;
- (void)start;
- (BOOL)isExecuting;

- (void)cancel;
- (BOOL)isCancelled;
- (BOOL)isFinished;

#pragma mark - Response blocks
- (void)addResponseBlock:(CTRequestResponseBlock)aResponseBlock responseQueue:(dispatch_queue_t)aResponseQueue;
- (void)clearAllResponseBlocks;

#pragma mark - Protected methods (use only in subclasses)
/**
 * Execute all response blocks asynchronously
 **/
- (void)executeAllResponseBlocksWithResponse:(CTResponse*)aResponse;
/**
 * Execute all response blocks synchronously
 **/
- (void)executeSynchronouslyAllResponseBlocksWithResponse:(CTResponse*)aResponse;

@end
