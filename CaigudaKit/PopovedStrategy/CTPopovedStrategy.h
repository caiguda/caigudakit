//
//  MUPopovedStrategy.h
//  CaigudaKit
//
//  Created by Malaar on 06.06.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTPopoverable.h"

@interface CTPopovedStrategy : NSObject

@property (nonatomic, assign) Class popoverBackgroundViewClass;

@property (nonatomic, weak, readonly) UIView* popovedView;
@property (nonatomic, retain, readonly) UIPopoverController *popoverController;
@property (nonatomic, assign, readonly) UIPopoverArrowDirection permittedArrowDirections;


- (void)presentPopoverFromView:(UIView*)aView
         withContentController:(UIViewController<CTPopoverable>*)aPopoverableController
      permittedArrowDirections:(UIPopoverArrowDirection)arrowDirections
                      animated:(BOOL)animated;

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation;

@end
