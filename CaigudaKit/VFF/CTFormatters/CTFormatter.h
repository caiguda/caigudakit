//
//  MUFormatter.h
//  CaigudaKit
//
//  Created by Malaar on 22.04.13.
//
//

#import <Foundation/Foundation.h>

@class CTFormatter;

@protocol CTFormatterProtocol <NSObject>

- (void)setFormatter:(CTFormatter*)aFormatter;
- (CTFormatter*)formatter;
- (NSString*)formattingText;

@end


@interface CTFormatter : NSObject

@property (nonatomic, weak) id<CTFormatterProtocol> formattableObject;

- (NSString*)formattedTextFromString:(NSString*)aOriginalString;
- (BOOL)formatWithNewCharactersInRange:(NSRange)aRange replacementString:(NSString*)aString;

- (NSString*)rawText;

@end
