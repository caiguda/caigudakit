//
//  CTValidators.h
//  CaigudaKit
//
//  Created by Malaar on 7/12/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#ifndef CTCaigudaKit_Validators
#define CTCaigudaKit_Validators

#import "CTValidator.h"
#import "CTValidationGroup.h"
#import "CTCompoundValidator.h"

#endif