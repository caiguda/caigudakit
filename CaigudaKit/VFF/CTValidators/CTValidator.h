//
//  CTValidator.h
//  CaigudaKit
//
//  Created by Malaar on 7/12/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@class CTValidator;

@protocol CTValidationProtocol <NSObject>

@property (nonatomic, assign) NSString* validatableText;

- (void)setValidator:(CTValidator*)aValidator;
- (CTValidator*)validator;
- (BOOL)validate;

@end

/**
 * Base validator
 **/
@interface CTValidator : NSObject
{
    __weak id<CTValidationProtocol> validatableObject;
    NSString *errorMessage;
}

@property (nonatomic, weak) id<CTValidationProtocol> validatableObject;
@property (nonatomic, strong) NSString *errorMessage;

- (BOOL)validate;

+ (NSString*)onlyNumbersStringFromText:(NSString*)aText;
+ (BOOL)isCharCanBeInputByPhonePad:(char)c;

@end


/**
 * Alwayse return YES
 **/
@interface CTValidatorAny : CTValidator
@end


/**
 * return YES for only numbers
 **/
@interface CTValidatorNumber : CTValidator
@end

/**
 * only english letters
 **/
@interface CTValidatorLetters : CTValidator
@end

/**
 * only english words
 **/
@interface CTValidatorWords : CTValidator
@end

/**
 * only email
 **/
@interface CTValidatorEmail : CTValidator
@end

/**
 * return YES if current value equal with value of testedValidator
 **/
@interface CTValidatorEqual : CTValidator
{
    CTValidator *testedValidator;
    BOOL isIgnoreCase;
}

- (id)initWithTestedField:(id<CTValidationProtocol>)aTestedObject;
- (id)initWithTestedFieldValidator:(CTValidator *)aTestedValidator;
@property(nonatomic,assign) BOOL isIgnoreCase;

@end

/**
 *
 **/
@interface CTValidatorNotEmpty : CTValidator
@end

/**
 *
 **/
@interface CTValidatorUSAZipCode : CTValidator
@end

/**
 *
 **/
/// full name consist of first name ' ' lastName
@interface CTValidatorFullName : CTValidator
@end

/**
 *
 **/
@interface CTValidatorURL : CTValidator
@end

/**
 *
 **/
///
@interface CTValidatorIntWithRange : CTValidator
{
    NSRange range;
}

- (id)initWithRange:(NSRange)aRange;

@end

/**
 *
 **/
@interface CTValidatorStringWithRange : CTValidator
{
    NSRange range;
}

- (id)initWithRange:(NSRange)aRange;

@end

/**
 * validation count number in phone number. Example: (050)-50-50-500
 **/
@interface CTValidatorCountNumberInTextWithRange : CTValidator
{
    NSRange range;
}

- (id)initWithRange:(NSRange)aRange;

@end


/**
 *
 **/
@interface CTValidatorMoney : CTValidator
@end

/**
 *
 **/
@interface CTValidatorRegExp : CTValidator

@property (nonatomic, retain) NSRegularExpression* regularExpression;

- (id)initWithRegExp:(NSRegularExpression*)aRegExp;

@end

/**
 *
 **/
@interface CTValidatorDomain : CTValidator
@end

