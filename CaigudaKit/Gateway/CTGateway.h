//
//  MUMGateway.h
//  CaigudaKit
//
//  Created by Malaar on 12.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFHTTPClient.h>
#import "CTGatewayRequest.h"
#import "CTGatewayRequestMultipart.h"
#import "CTGatewayConfigurator.h"
#import "CTResponse.h"

@interface CTGateway : NSObject
{
    AFHTTPClient* httpClient;
    NSMutableDictionary* defaultParameters;
}

@property (nonatomic, readonly) AFHTTPClient* httpClient;
@property (nonatomic, assign) BOOL disableRegisterInGatewayConfigurator;

#pragma mark - Internet reachability
- (BOOL)isInternetReachable;

#pragma mark - Configuration
- (void)configureWithBaseURL:(NSURL*)aBaseURL;

#pragma mark - Request generation
- (CTGatewayRequest*)requestForClass:(Class)aRequestClass
                            withType:(NSString*)aType
                                path:(NSString*)aPath
                          parameters:(NSDictionary*)aParameters;

- (CTGatewayRequest*)requestWithType:(NSString*)aType
                                path:(NSString*)aPath
                          parameters:(NSDictionary*)aParameters;

- (CTGatewayRequest*)requestWithType:(NSString*)aType
                                path:(NSString*)aPath
                          parameters:(NSDictionary*)aParameters
                        successBlock:(CTGatewayRequestSuccessBlock)aSuccessBlock
                       dispatchQueue:(dispatch_queue_t)aDispatchQueue;

- (CTGatewayRequest*)requestWithURLRequest:(NSURLRequest*)anURLRequest
                              successBlock:(CTGatewayRequestSuccessBlock)aSuccessBlock
                             dispatchQueue:(dispatch_queue_t)aDispatchQueue;

- (CTGatewayRequestMultipart*)multipartRequestWithType:(NSString*)aType
                                                  path:(NSString*)aPath
                                            parameters:(NSDictionary*)aParameters
                                          successBlock:(CTGatewayRequestSuccessBlock)aSuccessBlock
                                         dispatchQueue:(dispatch_queue_t)aDispatchQueue;


- (AFHTTPRequestOperation*)operationFromRequest:(CTGatewayRequest*)aRequest;

#pragma mark - Execute
- (AFHTTPRequestOperation*)startRequest:(CTGatewayRequest*)aRequest;
- (void)cancelAllRequests;

#pragma mark - Defaults
- (Class)defaultHTTPClientClass;
- (Class)defaultRequestClass;
- (CTGatewayRequestFailureBlock)defaultFailureBlockForRequest:(CTGatewayRequest*)aRequest;
- (void)addDefaultParameterValue:(NSString*)aValue forParameter:(NSString*)aParameter;
- (void)clearAllDefaultParameters;

@end
