//
//  MUMGateway.m
//  CaigudaKit
//
//  Created by Malaar on 12.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTGateway.h"
#import "CTKitDefines.h"

typedef void (^CTGatewayMultipartFormCallback) (id<AFMultipartFormData> aFormData);

@implementation CTGateway

@synthesize httpClient;

#pragma mark - Internet reachability

- (BOOL)isInternetReachable
{
    return [[CTGatewayConfigurator sharedInstance] isInternetReachable];
}

#pragma mark - Configuration

- (void)configureWithBaseURL:(NSURL*)aBaseURL
{
    NSParameterAssert(aBaseURL);
    Class httpClientClass = [self defaultHTTPClientClass];
    httpClient = [[httpClientClass alloc] initWithBaseURL:aBaseURL];
    defaultParameters = [NSMutableDictionary new];
}

#pragma mark - Request generation

- (CTGatewayRequest*)requestForClass:(Class)aRequestClass
                            withType:(NSString*)aType
                                path:(NSString*)aPath
                          parameters:(NSDictionary*)aParameters
{
    CTGatewayRequest* request = [[aRequestClass alloc] initWithGateway:self];
    request.type = aType;
    request.path = aPath;
    
    if(defaultParameters.count)
    {
        NSMutableDictionary* parameters = [NSMutableDictionary dictionaryWithDictionary:defaultParameters];
        [aParameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop)
         {
             [parameters setObject:obj forKey:key];
         }];
        request.parameters = parameters;
    }
    else
    {
        request.parameters = aParameters;
    }
    
    return request;
}

- (CTGatewayRequest*)requestWithType:(NSString*)aType
                                 path:(NSString*)aPath
                           parameters:(NSDictionary*)aParameters
{
    return [self requestForClass:[self defaultRequestClass] withType:aType path:aPath parameters:aParameters];
}

- (CTGatewayRequest*)requestWithType:(NSString*)aType
                                path:(NSString*)aPath
                          parameters:(NSDictionary*)aParameters
                        successBlock:(CTGatewayRequestSuccessBlock)aSuccessBlock
                       dispatchQueue:(dispatch_queue_t)aDispatchQueue
{
    CTGatewayRequest* request = [self requestWithType:aType path:aPath parameters:aParameters];
    CTGatewayRequestFailureBlock failureBlock = [self defaultFailureBlockForRequest:request];
    [request setupSuccessBlock:aSuccessBlock failureBlock:failureBlock dispatchQueue:aDispatchQueue];
    return request;
}

- (CTGatewayRequest*)requestWithURLRequest:(NSURLRequest*)anURLRequest
                              successBlock:(CTGatewayRequestSuccessBlock)aSuccessBlock
                             dispatchQueue:(dispatch_queue_t)aDispatchQueue
{
    CTGatewayRequest* request = [[[self defaultRequestClass] alloc] initWithGateway:self preparedURLRequest:anURLRequest];
    CTGatewayRequestFailureBlock failureBlock = [self defaultFailureBlockForRequest:request];
    [request setupSuccessBlock:aSuccessBlock failureBlock:failureBlock dispatchQueue:aDispatchQueue];
    return request;

}

- (CTGatewayRequestMultipart*)multipartRequestWithType:(NSString*)aType
                                                  path:(NSString*)aPath
                                            parameters:(NSDictionary*)aParameters
                                          successBlock:(CTGatewayRequestSuccessBlock)aSuccessBlock
                                         dispatchQueue:(dispatch_queue_t)aDispatchQueue;
{
    CTGatewayRequestMultipart* request = (CTGatewayRequestMultipart*)[self requestForClass:[CTGatewayRequestMultipart class]
                                                                                  withType:aType
                                                                                      path:aPath
                                                                                parameters:aParameters];
    CTGatewayRequestFailureBlock failureBlock = [self defaultFailureBlockForRequest:request];
    [request setupSuccessBlock:aSuccessBlock failureBlock:failureBlock dispatchQueue:aDispatchQueue];
    return request;
}

#pragma mark - Operation

- (AFHTTPRequestOperation*)operationFromRequest:(CTGatewayRequest*)aRequest
{
    NSMutableURLRequest* urlRequest = [aRequest urlRequest];
    AFHTTPRequestOperation* operation = [httpClient HTTPRequestOperationWithRequest:urlRequest
                                                                            success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [aRequest executeSuccessBlockWithOperation:operation responseObject:responseObject];
     
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        if(!operation.isCancelled)
            CTLog(@"\nCTGateway request failed with error:\n%@\n", error);
        [aRequest executeFailureBlockWithOperation:operation error:error];
    }];
    
    if (aRequest.inputStream)
        operation.inputStream = aRequest.inputStream;
    
    if (aRequest.outputStream)
        operation.outputStream = aRequest.outputStream;
    
    [operation setUploadProgressBlock:^(NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite)
    {
        [aRequest executeAllUploadProgressBlocksWithBytesWriten:bytesWritten
                                               totalBytesWriten:totalBytesWritten
                                      totalBytesExpectedToWrite:totalBytesExpectedToWrite];
    }];
    [operation setDownloadProgressBlock:^(NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead)
    {
        [aRequest executeAllDownloadProgressBlocksWithBytesRead:bytesRead
                                                 totalBytesRead:totalBytesRead
                                       totalBytesExpectedToRead:totalBytesExpectedToRead];
    }];

    return operation;
}

#pragma mark - Execute

- (AFHTTPRequestOperation*)startRequest:(CTGatewayRequest*)aRequest
{
    AFHTTPRequestOperation* operation = [self operationFromRequest:aRequest];
    [httpClient enqueueHTTPRequestOperation:operation];
    return operation;
}

- (void)cancelAllRequests
{
    [httpClient.operationQueue cancelAllOperations];
}

#pragma mark - Defaults
- (Class)defaultHTTPClientClass
{
    return [AFHTTPClient class];
}

- (Class)defaultRequestClass
{
    return [CTGatewayRequest class];
}

- (CTGatewayRequestFailureBlock)defaultFailureBlockForRequest:(CTGatewayRequest*)aRequest
{
    CTGatewayRequestFailureBlock block = ^(AFHTTPRequestOperation *operation, NSError *error)
    {
        CTResponse* response = [CTResponse new];
        response.error = error;
        response.code = error.code;
        response.requestCancelled = operation.isCancelled;
        return response;
    };
    return block;
}

- (void)addDefaultParameterValue:(NSString*)aValue forParameter:(NSString*)aParameter
{
    [defaultParameters setObject:aValue forKey:aParameter];
}

- (void)clearAllDefaultParameters
{
    [defaultParameters removeAllObjects];
}

@end
