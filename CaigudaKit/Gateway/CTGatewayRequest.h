//
//  MUGatewayRequest.h
//  CaigudaKit
//
//  Created by Malaar on 12.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTRequest.h"
#import <AFNetworking/AFNetworking.h>

@class CTGatewayRequest;
@class CTGateway;

typedef CTResponse* (^CTGatewayRequestSuccessBlock) (AFHTTPRequestOperation *operation, id responseObject);
typedef CTResponse* (^CTGatewayRequestFailureBlock) (AFHTTPRequestOperation *operation, NSError *error);

typedef void (^CTGatewayRequestUploadProgressBlock) (CTGatewayRequest* request, NSUInteger bytesWritten, long long totalBytesWritten, long long totalBytesExpectedToWrite);
typedef void (^CTGatewayRequestDownloadProgressBlock) (CTGatewayRequest* request, NSUInteger bytesRead, long long totalBytesRead, long long totalBytesExpectedToRead);


@interface CTGatewayRequest : CTRequest
{
    __weak CTGateway* gateway;
    __weak AFHTTPRequestOperation* operation;

    NSMutableArray* uploadProgressBlocks;
    NSMutableArray* downloadProgressBlocks;
    
    NSMutableDictionary* headers;
    
    NSURLRequest *preparedURLRequest;
}

@property (nonatomic, strong) NSString* path;
@property (nonatomic, strong) NSString* type;
@property (nonatomic, strong) NSDictionary* parameters;

@property (nonatomic, strong) NSInputStream *inputStream;
@property (nonatomic, strong) NSOutputStream *outputStream;

@property (nonatomic, readonly) CTGatewayRequestSuccessBlock successBlock;
@property (nonatomic, readonly) CTGatewayRequestFailureBlock failureBlock;

#if CT_NEEDS_DISPATCH_RETAIN_RELEASE
    @property (nonatomic, assign) dispatch_queue_t successFailureDispatchQueue;
#else
    @property (nonatomic, strong) dispatch_queue_t successFailureDispatchQueue;
#endif

#pragma mark - Init/Dealloc (for Gateway)
- (id)initWithGateway:(CTGateway*)aGateway;
- (id)initWithGateway:(CTGateway*)aGateway preparedURLRequest:(NSURLRequest*)anURLRequest;

#pragma mark - Internet reachability
- (BOOL)isInternetReachable;

#pragma mark - Headers
- (void)addValue:(NSString*)aHeaderValue forHeaderField:(NSString*)aHeaderField;
- (void)clearHeaders;

#pragma mark - Prepare request
- (NSMutableURLRequest*)urlRequest;

#pragma mark - Configure request callbacks (for Gateway)
- (void)setupSuccessBlock:(CTGatewayRequestSuccessBlock)aSuccessBlock
             failureBlock:(CTGatewayRequestFailureBlock)aFailureBlock
            dispatchQueue:(dispatch_queue_t)aDispatchQueue;

- (void)addUploadProgressBlock:(CTGatewayRequestUploadProgressBlock)anUploadProgressBlock
                 dispatchQueue:(dispatch_queue_t)aDispatchQueue;

- (void)addDownloadProgressBlock:(CTGatewayRequestDownloadProgressBlock)aDownloadProgressBlock
                   dispatchQueue:(dispatch_queue_t)aDispatchQueue;

#pragma mark - Execute request callbacks (for Gateway)
- (void)executeSuccessBlockWithOperation:(AFHTTPRequestOperation*)anOperation responseObject:(id)aResponseObject;
- (void)executeFailureBlockWithOperation:(AFHTTPRequestOperation*)anOperation error:(NSError*)anError;

- (void)executeAllUploadProgressBlocksWithBytesWriten:(NSUInteger)aBytesWritten
                                     totalBytesWriten:(long long)aTotalBytesWritten
                            totalBytesExpectedToWrite:(long long)aTotalBytesExpectedToWrite;

- (void)executeAllDownloadProgressBlocksWithBytesRead:(NSUInteger)aBytesRead
                                       totalBytesRead:(long long)aTotalBytesRead
                             totalBytesExpectedToRead:(long long)aTotalBytesExpectedToRead;

@end
