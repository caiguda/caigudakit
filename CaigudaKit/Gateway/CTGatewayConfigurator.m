//
//  CTGatewayConfigurator.m
//  CaigudaKit
//
//  Created by Malaar on 24.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTGatewayConfigurator.h"
#import "CTGateway.h"
#import <objc/runtime.h>

@implementation CTGatewayConfigurator

CT_IMPLEMENT_SINGLETON(CTGatewayConfigurator);

- (id)init
{
    self = [super init];
    if(self)
    {
        [AFNetworkActivityIndicatorManager sharedManager].enabled = YES;
        
        gateways = [NSMutableArray new];
        reachability = [Reachability reachabilityForInternetConnection];
//        [reachability startNotifier];

        int classesCount = objc_getClassList(NULL, 0);
        Class* classes;
        
        if(classesCount > 0)
        {
            classes = (__unsafe_unretained Class*)malloc(sizeof(Class) * classesCount);
            objc_getClassList(classes, classesCount);
            
            Class cls;
            Class subCls;
            NSMutableArray* filteredClasses = [NSMutableArray array];
            for(int i = 0; i < classesCount; ++i)
            {
                cls = classes[i];
                subCls = cls;
                do
                {
                    subCls = class_getSuperclass(subCls);
                    if(subCls == [CTGateway class] && class_getClassMethod(cls, @selector(sharedInstance)))
                    {
                        [filteredClasses addObject:NSStringFromClass(cls)];
                        break;
                    }
                } while (subCls);
            }
            free(classes);
            
            CTGateway* gateway;
            for(NSString* className in filteredClasses)
            {
                cls = NSClassFromString(className);
                gateway = (CTGateway*)[cls sharedInstance];
                [self registerGateway:gateway];
            }
        }

    }
    return self;
}

#pragma mark - Gateway registration/configuration

- (void)registerGateway:(CTGateway*)aGateway
{
    NSParameterAssert(aGateway);
    if(!aGateway.disableRegisterInGatewayConfigurator)
        [gateways addObject:aGateway];
}

- (void)configureGatewaysWithBaseURL:(NSURL*)aBaseURL
{
    NSParameterAssert(aBaseURL);
    for(CTGateway* gateway in gateways)
        [gateway configureWithBaseURL:aBaseURL];
}

#pragma mark - Internet reachability

- (BOOL)isInternetReachable
{
    return [reachability isReachable];
}
@end
