//
//  CTGatewayConfigurator.h
//  CaigudaKit
//
//  Created by Malaar on 24.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Reachability/Reachability.h>
#import "CTSingleton.h"

@class CTGateway;

@interface CTGatewayConfigurator : NSObject
{
    NSMutableArray* gateways;
    Reachability* reachability;
}

CT_DECLARE_SINGLETON(CTGatewayConfigurator);

#pragma mark - Gateway registration/configuration
- (void)registerGateway:(CTGateway*)aGateway;
- (void)configureGatewaysWithBaseURL:(NSURL*)aBaseURL;

#pragma mark - Internet reachability
- (BOOL)isInternetReachable;

@end
