//
//  CTGatewayRequestMultipart.h
//  CaigudaKit
//
//  Created by Malaar on 24.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "CTGatewayRequest.h"

@protocol CTMultipartFormData <AFMultipartFormData>
@end

typedef void (^CTConstructingMultipartFormDataBlock)(id<CTMultipartFormData> formData);

@interface CTGatewayRequestMultipart : CTGatewayRequest
{
    NSMutableArray* constructingBlocks;
}

- (void)addConstructingMultipartFormDataBlock:(CTConstructingMultipartFormDataBlock)block;

@end


