//
//  CTKitDefines.h
//  CaigudaKit
//
//  Created by Malaar on 9/23/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#ifndef CaigudaKit_CTKitDefines_h
#define CaigudaKit_CTKitDefines_h

#define CT_NULL_PROTECT(value) ( ((NSNull*)value == [NSNull null]) ? (nil) : (value) )
#define CT_NIL_PROTECT(value) ( (value == nil) ? ([NSNull null]) : (value) )

#define CT_IS_IPAD ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad)

#define CT_CHECK_INDEX(index, min, max) {NSAssert( index >= min && index < max, @"Wrong index!");}

#define CT_IS_RETINA ([UIScreen mainScreen].scale > 1.5f)

/**
 * Log
 **/

#ifdef DEBUG
#   define CTLog(format, ...) NSLog(format, __VA_ARGS__)
#else
#   define CTLog(format, ...)
#endif

#define CT_IS_IPHONE_FOUR_INCH ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone && \
                                MAX([UIScreen mainScreen].bounds.size.height, [UIScreen mainScreen].bounds.size.width) == 568.0)

#define CT_IS_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
#define CT_IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

/**
 * Dispatch retain/release
 **/

#define CT_NEEDS_DISPATCH_RETAIN_RELEASE !OS_OBJECT_USE_OBJC

/**
 * Autoresizing masks
 **/

#define CTViewAutoresizingFlexibleSize      UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth
#define CTViewAutoresizingFlexibleMargin    UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleBottomMargin

/*
 *  System Versioning Preprocessor Macros
 */

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define CTSystemVersionIsAbove5x                    SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5.0")
#define CTSystemVersionIsAbove6x                    SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"6.0")

#endif
