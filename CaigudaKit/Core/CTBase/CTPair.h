//
//  CTPair.h
//  CaigudaKit
//
//  Created by Malaar on 10/13/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTPair : NSObject

@property (nonatomic, strong) id first;
@property (nonatomic, strong) id second;

+ (CTPair*)pairWithFirst:(id)aFirst second:(id)aSecond;
- (id)initWithFirst:(id)aFirst second:(id)aSecond;

@end


@interface NSArray (CTPair)

- (CTPair*)pairByFirst:(id)aFirst;
- (CTPair*)pairBySecond:(id)aSecond;

@end