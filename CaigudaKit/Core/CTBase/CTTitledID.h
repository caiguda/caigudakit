//
//  MUBOTitledID.h
//  CaigudaKit
//
//  Created by Malaar on 9/23/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CTTitledID : NSObject <NSCopying>
{
    id ID;
    NSString* title;
}

@property (nonatomic, strong) id ID;
@property (nonatomic, strong) NSString* title;

- (id)initWithID:(id)aID title:(NSString*)aTitle;

@end


@interface NSArray (CTTitledID)

- (CTTitledID*)titledIDByID:(id) aID;
- (CTTitledID*)titledIDByTitle:(NSString*) aTitle;

@end