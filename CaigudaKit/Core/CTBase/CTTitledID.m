//
//  MUBOTitledID.m
//  CaigudaKit
//
//  Created by Malaar on 9/23/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import "CTTitledID.h"


@implementation CTTitledID

@synthesize ID;
@synthesize title;

- (id)initWithID:(id)aID title:(NSString*)aTitle
{
    if( (self = [super init]) )
    {
        self.ID = aID;
        self.title = aTitle;
    }
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    return [[[self class] allocWithZone:zone] initWithID:ID title:title];
}

- (NSString*)description
{
    return [NSString stringWithFormat:@"{%@, %@}", ID, title];
}

@end

@implementation NSArray (CTTitledID)

- (CTTitledID*)titledIDByID:(id) aID
{
    CTTitledID* result = nil;
    
    if([aID isKindOfClass:[NSString class]])
    {
        for(CTTitledID* titledID in self)
        {
            if([titledID.ID isEqualToString:aID])
            {
                result = titledID;
                break;
            }
        }
    }
    else
    {
        for(CTTitledID* titledID in self)
        {
            if([titledID.ID isEqual:aID])
            {
                result = titledID;
                break;
            }
        }
    }

    return result;
}

- (CTTitledID*)titledIDByTitle:(NSString*) aTitle
{
    CTTitledID* result = nil;
    
    for(CTTitledID* titledID in self)
    {
        if([titledID.ID isEqualToString:aTitle])
        {
            result = titledID;
            break;
        }
    }
    
    return result;
}

@end