//
//  MUHelper.h
//  Pro-Otdyh
//
//  Created by Malaar on 24.03.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CTExtensions.h"

inline UIViewController* CTGetPrimeViewController();

void CTShowSimpleAlert(NSString* aTitle, NSString* aMessage);

typedef BOOL (^CTDividedComparator)(id anObj1, id anObj2);
NSMutableArray* CTDivideArray(NSArray* aDividedArray, NSString* aFieldName, BOOL anAscending, CTDividedComparator aComparator);

NSDate* CTDateFromTimeStampInDictionary(NSDictionary* aDictionary, NSString* aKey);

inline NSString* CTGenerateUUID();
inline NSString* CTDocumentDirectoryPath();
inline NSString* CTCacheDirectoryPath();

#define CTFrameFromSize(size) CGRectMake(0, 0, size.width, size.height);
