//
//  CTSingleton.h
//  CaigudaKit
//
//  Created by Malaar on 24.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

#define CT_DECLARE_SINGLETON(className)                     \
+ (className*)sharedInstance;

#define CT_IMPLEMENT_SINGLETON(className)                   \
+ (className*)sharedInstance                                \
{                                                           \
    static className *sharedInstance = nil;                 \
    static dispatch_once_t onceToken;                       \
    dispatch_once(&onceToken, ^                             \
    {                                                       \
        sharedInstance = [className new];                   \
    });                                                     \
    return sharedInstance;                                  \
}

