//
//  MUFetchable.h
//  CaigudaKit
//
//  Created by Malaar on 10/14/11.
//  Copyright 2011 Caiguda. All rights reserved.
//

#import <Foundation/Foundation.h>

//==============================================================================
@protocol CTFetchable <NSObject>

/**
 * Need fetch data from server
 **/
@property (nonatomic, assign) BOOL needFetch;

- (void)fetchData;

@end
