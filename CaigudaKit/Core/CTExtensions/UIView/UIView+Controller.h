//
//  UIView+Controler.h
//  CaigudaKit
//
//  Created by Malaar on 11.07.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Controller)

- (UIViewController*)firstAvailableUIViewController;
- (id)traverseResponderChainForUIViewController;

@end
