//
//  UIView+Controler.m
//  CaigudaKit
//
//  Created by Malaar on 11.07.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "UIView+Controller.h"

@implementation UIView (Controller)

- (UIViewController *)firstAvailableUIViewController
{
    // convenience function for casting and to "mask" the recursive function
    return (UIViewController *)[self traverseResponderChainForUIViewController];
}

- (id)traverseResponderChainForUIViewController
{
    id result = nil;
    
    id nextResponder = [self nextResponder];
    if ([nextResponder isKindOfClass:[UIViewController class]])
        result = nextResponder;
    else if ([nextResponder isKindOfClass:[UIView class]])
        result = [nextResponder traverseResponderChainForUIViewController];
    
    return result;
}

@end
