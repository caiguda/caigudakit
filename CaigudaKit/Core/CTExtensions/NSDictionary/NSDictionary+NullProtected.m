//
//  NSDictionary+NullProtected.m
//  CaigudaKit
//
//  Created by Malaar on 24.04.13.
//  Copyright (c) 2013 Caiguda. All rights reserved.
//

#import "NSDictionary+NullProtected.h"
#import "CTKitDefines.h"

@implementation NSDictionary (NullProtected)

- (id)nullProtectedObjectForKey:(id)aKey
{
    return CT_NULL_PROTECT(self[aKey]);
}

- (id)nullProtectedObjectForKeyPath:(id)aKeyPath
{
    return CT_NULL_PROTECT([self valueForKeyPath:aKeyPath]);
}

- (NSDictionary*)dictionaryCleanedFromNulls
{
    NSSet* keySet = [self keysOfEntriesPassingTest:^BOOL(id key, id obj, BOOL *stop)
    {
        return ![obj isEqual:[NSNull null]];
    }];
    
    return [self dictionaryWithValuesForKeys:[keySet allObjects]];
}

@end


@implementation NSMutableDictionary (NilProtected)

- (void)setNilProtectedObject:(id)anObject forKey:(id<NSCopying>)aKey
{
    self[aKey] = CT_NIL_PROTECT(anObject);
}

@end