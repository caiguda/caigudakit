//
//  MUDraw.h
//  CaigudaKit
//
//  Created by Malaar on 04.05.12.
//  Copyright (c) 2012 Caiguda. All rights reserved.
//

#ifndef CTDraw_h
#define CTDraw_h

#import <QuartzCore/QuartzCore.h>

extern void CTDrawLinearGradient(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor, bool isHorizontal);
extern void CTDrawLinearGradientVertical(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor);
extern void CTDrawLinearGradientHorizontal(CGContextRef context, CGRect rect, CGColorRef startColor, CGColorRef endColor);

extern CGContextRef CTCreateThreadSafeContext(CGSize contextSize);
extern CGImageRef CTCreateCGImageFromThreadSafeContext(CGContextRef context);

extern UIImage* CTImageWithColor(UIColor* color, CGSize size);

#endif
